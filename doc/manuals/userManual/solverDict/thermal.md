# Thermal Solution {#thermal}

The <code><b>thermalSubSolver</b></code> class is used to handle the modeling of heat transfer in OFFBEAT.

The main field is the temperature which by default is named <code><b>T</b></code> and is in <code><b>K</b></code>. 

### Usage
The thermal solver <u>must</u> be selected with the <code><b>thermalSolver</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code>  dictionary, located in the <code><b>constant</b></code>  folder). 

Currently OFFBEAT supports the following thermal solvers:

- [fromLatestTime](@ref Foam.thermalSubSolver) - deactivates the thermal solution (i.e. no equation is solved) and the temperature <code><b>T</b></code> file is defined in the starting time folder;
- [readTemperature](@ref Foam.readTemperature) - at each time-step the temperature field is read from a sub-folder in the <code>constant/</code> folder;
- [solidConduction](@ref Foam.solidConductionSolver) - solves the heat conduction equation with the possibility of adding a volumetric source <code><b>Q</b></code> (see [heatSource class](heatSource.html));

***

Return to [User Manual](@ref userManual)