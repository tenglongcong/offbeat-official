# Fast Flux and Fast Fluence Models {#fastFlux}

The <code><b>fastFlux</b></code> class is used to enable the modeling of fast flux and fast fluence in OFFBEAT.
The two fields are by default named <code><b>fastFlux</b></code> and <code><b>fastFluence</b></code>, and are in <c>n/cm<sup>2</sup>/s</c> and <c>n/cm<sup>2</sup></c>, respectively. 

### Usage
The fast flux model <u>must</u> be selected with the <code><b>fastFlux</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code>  dictionary, located in the <code><b>constant</b></code>  folder). 

Currently OFFBEAT supports the following fast flux models:

- [none](@ref Foam.fastFlux) - it neglects fast flux and fast fluence, i.e. the two fields are not created;
- [fromLatestTime](@ref Foam.constantFastFlux) - the fast flux field is read from the <code><b>fastFlux</b></code> file in the starting time folder (the fast fluence evolves accordingly over time);
- [timeDependentAxialProfile](@ref Foam.timeDependentAxialProfile) - it prescribes a rod-average fast flux that changes over time with the possibility of adding a time-dependent axial profile.

***

Return to [User Manual](@ref userManual)