# TRISO {#tutorials_TRISO}

TRISO (Tristructural Isotropic) fuel is an advanced type of nuclear fuel designed for high-temperature gas-cooled reactors. It consists of small spherical fuel kernels encapsulated in multiple layers of coatings. These layers provide a robust defense against the release of radioactive materials, even under extreme conditions, making TRISO fuel highly attractive for next-generation nuclear reactors due to its enhanced safety features and improved fuel performance.

To simulate the behavior of TRISO during reactor operation, OFFBEAT has been extended to accommodate the TRISO case. The material properties of the porous carbon buffer, PyC (Pyrolytic Carbon), and SiC (Silicon Carbide) have been implemented into the code, along with the behavior models of these materials, which include anisotropic irradiation dimension change, thermal expansion, and creep.

### Mesh

OFFBEAT supports the simulation of TRISO in 1-D, 2-D, and 3-D geometry, and it provides tools to automatically create blockMesh. This allows for versatile modeling options and simplifies the process of setting up simulations

#### 1-D Mesh

To simulate the TRISO in 1D, the **Wedge** boundary conditions are used for circumferential directions, resulting in a mesh shape resembling a rectangular pyramid.

\image html 1D_TRISO.png "TRISO 1 Dimensional Model" width=50%

OFFBEAT provides a Python script to facilitate geometry creation and meshing. The script, along with its input file, can be found at ./offbeat/tools/sphereMaker_1D.

In the input file named sphereDict, users can specify the parameters for the geometry.

To model specific parts of the TRISO, the following command can be used. This command will build a fuel TRISO model with Kernel, Buffer, IPyC, SiC and OPyC.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
'Kernel':             True,
'Buffer':             True,
'IPyC':               True,
'SiC':                True,
'OPyC':               True,
</code></pre>

</div>

The angle of the wedge can be specified by `wedgeAngle`:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	'wedgeAngle':         2,
</code></pre>
</div>

The users can also specify the default gap between Buffer and IPyC.
<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	'gap':                0.0,
</code></pre>
</div>


<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br>Setting the `gap` to 0.0 means there's no physical gap between Buffer and IPyC layers. However, it's important to note that these two layers are still considered detached from each other, similar to the assumption made in many other codes.
</div>

The unit conversion can be specified by `convertToMeters`, where 1e-6 means the unit is in um.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	'convertToMeters':    1e-6,
</code></pre>
</div>

At last, users can define the dimensions of different layers as well as the number of elements for each layer. For example, the following input will create a TRISO mesh with a Kernel (radius 251 um, and 12 elements), Buffer(thickness 95 um and 12 elements), etc.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	'rKernel':          	251.0,
	'meshKernel':           12,

	'tBuffer':          	95.0,
	'meshBuffer':           12,

	'tIPyC':          	  41.0,
	'meshIPyC':             12,

	'tSiC':          	    35.0,
	'meshSiC':               16,

	'tOPyC':          	  40.0,
	'meshOPyC':             12,
</code></pre>
</div>

The script will generate blocks named Kernel, Buffer, IPyC, SiC and OPyC, with patches named bufferOuter, ipycInner, outer, front, back, top and bottom. The radial direction of the 1D mesh will be along the X axis.

#### 3-D Mesh
OFFBEAT also provides another script to create an octant 3-D sphere. The script shares the similar input as the 1-D mesh script, but it requires three additional parameters. The script is located at ./offbeat/tools/sphereMaker_3D.

* `ratioOtype` - The ratio of O-type mesh over the whole Kernel mesh.
* `ratio` - The ratio of O-type mesh's edge length at X, Y, Z axis over its original radio.
* `hoopMesh` - 2 times of this value will be used for the cell number along circumferential directions.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	'ratioOtype':         0.7,
	'ratio':              0.8,
	'hoopMesh':           20,
</code></pre>
</div>


\image html 3D_TRISO.png "TRISO 1 Dimensional Model" width=20%

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br>For 3D mesh, only TRISO with all layers can be generated.
</div>

### Boundary Conditions & Initial Conditions

The TRISO simulation typically requires 2 initial fields, **DD** and **T, to be provided at `/`0` folder, along with a uniform value **gapGas** at `/0/uniform` folder. The following part will give an example to show how it works in 1-D cases.

#### DD
**DD** represents the displacement increment. For the wedged patches, we only need to specify their type by "wedge".

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	"bottom|top|front|back"
	{
			type            wedge;
	}
</code></pre>
</div>

The "bufferOuter" and "ipycInner" are usually coupled since they can have a contact between them.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	"bufferOuter|ipycInner"
	{
			type            gapContact;
			patchType       regionCoupledOFFBEAT;
			penaltyFactor   0.0;
			frictionCoefficient        0;
			relax           1.0;
			relaxInterfacePressure           0.1;
			traction        uniform (0 0 0);
			pressure        uniform 0.0;
			value           $internalField;
	}
</code></pre>
</div>

The "outer" is usually assigned a constant pressure.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	outer
	{
			type            tractionDisplacement;
			traction        uniform (0 0 0);
			pressure        uniform 0.1e6;
			value           uniform (0 0 0);
	}
</code></pre>
</div>

#### T

The input for the wedged patches is the same as **DD**. Additionally, the "bufferOuter" and "ipycInner" patches are coupled using `trisoGap` to account for heat transfer between them.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; overflow: scroll; height: 200px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	bufferOuter
	{
		type            trisoGap;
		patchType       regionCoupledOFFBEAT;
		kappa           k;
		coupled         true;
		roughness       uniform 1e-6;
		jumpDistance    uniform 0.0;
		value           $internalField;
		relax           1;
	}
	//
	ipycInner
	{
		type            trisoGap;
		patchType       regionCoupledOFFBEAT;
		kappa           k;
		coupled         true;
		roughness       uniform 1e-6;
		jumpDistance    uniform 0.0;
		value           $internalField;
		relax           1;
	}
</code></pre>
</div>

The "outer" is usually given a constant temperature.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	outer
	{
		type            fixedValue;
		value           uniform 1608;
	}
</code></pre>
</div>

#### gapGas

The gapGas represents the gas present between the "bufferOuter" and "ipycInner" layers, as well as inside the buffer. At the initial stage, `massFractions` and `gasPressure` are both set to 0 since there's no gap and no filling gas. `gasPressureType` is set to **fromModel** so that the quantity of these gas can be calculated by corresponding models. For more information about the model, users can refer to [gapTRISO](@ref Foam.gapTRISO)

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin:0;"><code>
	massFractions
	{
	    Ar 0;
	    He 0;
	    Kr 0;
	    Ne 0;
	    Rn 0;
	    Xe 0;
	    CO 0;
	};

	gasPressureType                 fromModel;
	gasPressure                     0;
</code></pre>
</div>

### solverDict

The solverDict for TRISO is generally similar to every other OFFBEAT solverDict, except for several special points.

The `mechanicsSolver` usually use **smallStrainIncrementalUpdated** because the shrinkage of **Buffer** during irradiation can be significant.

**timeDependentVhgr** is usually used for volumetric `heatsource` and **TRISO** is typical type of `gapGas` but it also includes the compentence for CO production.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; overflow: scroll; height: 200px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
//- Thermal and Mechanical solver selection:
thermalSolver           solidConduction;
mechanicsSolver         smallStrainIncrementalUpdated;
neutronicsSolver        fromLatestTime;
elementTransport        fromLatestTime;

//- Material and rhelogy treatment:
materialProperties      byZone;
rheology                byMaterial;

heatSource              timeDependentVhgr;
burnup                  fromPower;
fastFlux                timeDependentAxialProfile;
corrosion               fromLatestTime;
gapGas                  TRISO;
fgr                     SCIANTIX;
</code></pre>
</div>

If the simulation is in 3D, then the user should provide `angularFraction` in the `globalOptions` dictionary. For example, the `angularFraction` of an octant sphere is **0.125**.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
globalOptions
{
    angularFraction          0.125;
}
</code></pre>
</div>

In the `mechanicsSolverOptions` dictionary, the 1-D case can use **uniform** with a `defaultWeights` of **1** for `multiMaterialCorrection`.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; overflow: scroll; height: 200px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
//For 1D
mechanicsSolverOptions
{
     forceSummary        off;
     multiMaterialCorrection
     {
         type                    uniform;
         defaultWeights          1;
     }
}
</code></pre>
</div>

As for 3-D case, it is recommended to use **cellZone** with the `interfaceWeights` set to **1** and `defaultWeights` set to **0.9**. A lower value of `defaultWeights` results in faster simulations but may yield less precise results. Additionally, the `sphericalStress` can be enabled to generate the radial and circumferential stress, strain and displacement fields.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; overflow: scroll; height: 200px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
//For 3D
mechanicsSolverOptions
{
    forceSummary        off;
    sphericalStress     on;
    multiMaterialCorrection
    {
        type                    cellZone;
        interfaceWeights        1;
        defaultWeights          0.9;
    }
}
</code></pre>
</div>

The `materials` dictionary usually contains 4 blocks of materials to represent different regions of TRISO: **Kernel**(or **UO2**), **Buffer**, **IPyC|OPyC** and **SiC**. Each of the materials has a corresponding class that defines all of its material properties and behaviors. Users are encouraged to check the use of different materials:

* [UO2](@ref Foam.UO2)
* [Buffer](@ref Foam.buffer)
* [PyC](@ref Foam.PyC)
* [SiC](@ref Foam.SiC)

Special care should be taken for the `swellingModel` of **UO2**. Currently, the SCIANTIX in OFFBEAT is not perfectly suitable for TRISO case and the fission gas swelling provided by SCIANTIX may not be reliable. As a result, it is strongly recommended to use **UO2MATPRO** for the swelling model.

***

Return to [Tutorials](@ref tutorials)
