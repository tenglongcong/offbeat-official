# TRISO {#tutorials_TRISO_FPT}

The fission products (FP) such as Cs, Ag and Kr  generated inside the kernels can migrate through layers and release to the matrix and eventually to the coolant. Therefore, accurately predicting how these fission products move within the layers and their release rates is also an essential part for TRISO fuel simulation.

The migration of fission products inside the TRISO layers are governed by the Fick’s law with the effective diffusion coefficient. The theory of it can be found in [fissionProductsDiffusionSolver](@ref Foam.fissionProductsDiffusionSolver).

The general setting for TRISO simulation can be found in [Thermomechanics](@ref tutorials_TRISO_thermalmechanics). This documentation focuses on the specific settings for FP transport.

### Mesh

Typically, there's no gap between layers for the simulation of FP transport.

### Boundary Conditions & Initial Conditions

No special settings are required for the boundary and initial conditions of FP quantities. OFFBEAT will automatically generate these fields. You can specify the initial concentration of FP within the 'solverDict,' and the boundary conditions will be automatically set to 0.

### solverDict

The `elementTransport`should be switch on to **byList**.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  elementTransport        byList;
</code></pre>
</div>

Next within `elementTransportOptions`, set the `solvers` to **FpDiffusion**. Inside the dictionary `FpDiffusionOptions`, you need to define 2 parameters.

* `outerPatches` - The name of the boundary patche. The default name is **outer**.
* `intialFpConcentration` - The initial Fp concentration within the Kernel. Pleases note the fields for FP inside this dictionary will be generated automatically.

If a heat source is provided, then the production of these FP will be calculated within the Kernel. Otherwise there will be no proudcion of FP. Currently, the production rate is available for Cs, Kr (Kr+Xe), Ag and Sr.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; overflow: scroll; height: 200px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
elementTransportOptions
{
  solvers
  (
      FpDiffusion
  );
  FpDiffusionOptions
  {
    outerPatches   outer;
    intialFpConcentration
    {
      Cs       0;
      Ag       0;
    }
  }
}
</code></pre>
</div>

If the simulations invovle FP "Cs, Kr, Ag and Sr" within materials "UO2, PyC, SiC and graphite",  no additional settings are required for the simulation. Alternatively, you can specify the `diffCoefModel` within material dictionary. The exsisted models can be found in [diffCoefModel](@ref Foam.diffCoefModel).

If the you want to customize diffusion coefficient for any type of FP, you can define the coefficent of each FP within the 'fissionProductsTransport' dictionary.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; overflow: scroll; height: 200px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  fissionProductsTransport
  {
    fissionProductsName  ("Cs" "Ag");
    Cs
    {
      d1        1e-8;
      d2        0;
      g1        0;
      g2        0;
    }
    Ag
    {
      d1        1e-8;
    }
  }
</code></pre>
</div>

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br>User-provided values take precedence. For instance, if the PyC model is applied, but the diffusion coefficient of Cs is manually set to 1e-6, then the value of 1e-6 will be used instead of relying on the Cs correlation within PyC.
</div>

### Examples
The benchmark cases for IAEA CRP-6 tests can be found in [trisoFpDiffusion](https://gitlab.com/AlessandroScolaro/offbeat-official/-/tree/development/TRISO/Cases/Verification/trisoFpDiffusion?ref_type=heads)

***

Return to [Tutorials](@ref tutorials)
