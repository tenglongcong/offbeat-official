# Zircaloy material model {#zircaloy}

Material model for Zircaloy material (see zircaloy.H). The model uses default selections for thermomechanical properties and behavioral models, but different choices can be selected by the user. 

The default selections of the material model are listed in the table below.

Please refer to the header of each listed file for more information on the specific correlation/model.

### Thermomechanical properties

| Thermomechanical property | Default model                 |
|---------------------------|-------------------------------|
| Thermal Conductivity      | densityIAEAZy.H               |
| Density                   | heatCapacityIAEAZy.H          |
| Emissivity                | constantEmissivityZy.H        |
| Heat Capacity             | heatCapacityIAEAZy.H          |
| Poisson's Ratio           | constantPoissonRatioZy.H      |
| Thermal Expansion         | thermalExpansionMatproZy.H    |
| Young's Modulus           | YoungModulusMatproZy.H        |

### Behavioral models

| Behavioral phenomenon     | Default model                 |
|---------------------------|-------------------------------|
| Swelling                  | swellingGrowthBISONZy.H       |
| PhaseTransition           | phaseTransitionZyDynamic.H    |
| Failure                   | failureModel.H (i.e. "none")  |
