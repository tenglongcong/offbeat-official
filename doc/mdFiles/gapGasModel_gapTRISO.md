### User documentation for `gapTRISO` class {#gapGasModel_gapTRISO}

Model for gap gas composition, volume, temperature and pressure for TRISO.

#### Formulation

The main assumptions are:
- ideal gas law
- each region of the free volume has different V and T
- the pressure is instantaneously equalized everywhere in the free volume

The gas pressure is going to be calculated as:

$$
\begin{aligned}
p = \frac{nR}{sum(V_i/T_i)}
\end{aligned}
$$

where the i might stand for the gap, porous volume of Buffer, central hole or cracks. The calculation of the various V/T terms is done separately for the various
section of the free volume.

The calculation of the gap and plena volumes is done applying the gauss-green theorem applied to the bounding surfaces:

$$
\begin{aligned}
volume = \int_V(dV) = 1/3\int_Vdiv(r)dV = 1/3\int_S(S\mathbf{n} dS)
\end{aligned}
$$

NOTE: the volumes are in m3, and refer to the modeled geometry.
e.g. Consider a full sphere plenum of 36 cm3. If the sphere is reproduced
with a 2-degree wedge mesh, its fraction compared with the whole sphere is

$$
\begin{aligned}
fraction = \frac{(2\pi/180)^2}{4\pi}=9.696273622\cdot10^{-5}
\end{aligned}
$$

So the gap volume in this model is

$$
\begin{aligned}
V_{model} = 36\times10^{-6}\times9.696273622\times10^{-5} m^3 = 3.49\times10^{-9} m^3
\end{aligned}
$$

The production of CO is also included in this class, the current models are GA model and Proksch model.

##### GA model

$$
\begin{aligned}
CO_{prod}=min(0.625, 1.64e^{\frac{-3311}{T}})
\end{aligned}
$$

where,
- \f$CO_{prod}\f$ is produced CO per fission(atoms/fission),
- \f$T\f$ is temperature(K),
- 0.625 is the limit set by PARFUME.

##### Proksch model

$$
\begin{aligned}
log\frac{O/F}{t^2}=-0.21-\frac{8500}{T}
\end{aligned}
$$

where,
- \f$O/F\f$ is oxygen release at the end of irradiation(atoms/fission),
- \f$t\f$ is irradiation time(days),
- \f$T\f$ is time-averaged particle surface temperature(K).

The maximum value of \f$O/F\f$ is limited by the following expression:

$$
\begin{aligned}
(O/F)_{max}=0.40f_U+0.85f_{Pu}
\end{aligned}
$$

where \f$f_U\f$ and \f$f_{Pu}\f$ are the fission fractions of \f$^{235}U\f$ and \f$^{239}Pu + ^{241}Pu\f$, respectively.

We assume that all the oxygen forms carbon monoxide, then \f$CO_{prod}=O/F\f$.

#### Usage

To use `gapTRISO` in OFFBEAT, you need to specify the keyword **TRISO** for `gapGas`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  gapGas                  TRISO;
</code></pre>
</div>

Then in the `gapGasOptions` dictionary:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; overflow: scroll; height: 200px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
//- Thermal and Mechanical solver selection:
  gapGasOptions
  {
      gapPatches ( bufferOuter ipycInner );
      bufferOuterPatches bufferOuter;
      bufferPorosity  0.4054;
      bufferZones     Buffer;
      COProductionModel  Proksch;
      model3D    false;
      gapVolumeOffset 0.0;
      gasReserveVolume 0.0;
      gasReserveTemperature 290;
      // // Alternatively one can provide a time dependent list
      // gasReserveTemperatureList
      // {
      //      type    table;
      //
      //        values
      //        (
      //            (0.0    290)
      //            (100.0  590)
      //        );
      //
      //        outOfBounds         clamp;     // optional out-of-bounds handling
      //        interpolationScheme linear;    // optional interpolation method
      // }
  }

</code></pre>
</div>

In `gapGas file` inside the time step folder (e.g. 0/):

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; overflow: scroll; height: 200px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
massFractions
{
    Ar 0;
    He 0;
    Kr 0;
    Ne 0;
    Rn 0;
    Xe 0;
    CO 0;
};

gasPressureType                 fromModel;// fixed;// fromList;
gasPressure                     0;

//  gapPressureList
//  {
//  type    table;
//
//      values
//      (
//          (0.0    1e5)
//          (100.0  1e6)
//      );

//      outOfBounds         clamp;     // optional out-of-bounds handling
//      interpolationScheme linear;    // optional interpolation method
//  }

</code></pre>
</div>

#### Input parameters

|                |   Description                   |     Default            |Options|
|----------------|---------------------------------|------------------------|--------------|
|heatSourceName |Name of heat source field  |Q       | |
|COProductionModel |CO production model       |Proksch | Proksch<br />GA |
|model3D | Is the model in 3D |false | |
|gapVolumeOffset |Gap volume offset(m³) |**Required** | |
|gasReserveVolume |Additional gas reserve volume(m³) |**Required** | |
|bufferPorosity |The porosity of buffer |**Required** | |
|gasReserveTemperature <br /> gasReserveTemperatureList|Additional gas reserve temperature(K) |**Required** | |
