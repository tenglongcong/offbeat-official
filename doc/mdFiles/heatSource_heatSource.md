### User documentation for `fromLatestTime` heatSource class {#heatSource_heatSource}

For general instructions on the modeling of a heat source in OFFBEAT see [here](heatSource.html).

The `fromLatestTime` heatSource class in OFFBEAT allows the user to set the heat source field in a file located the starting time folder. If the <C>Q</C> file is present in the starting time folder, the internal field and boundary conditions are set according to the <C>Q</C> file. Otherwise, the heat source field is set to 0 <c>W/m<sup>3</sup></c> in each cell, with zeroGradient BCs in all non-empty/-wedge patches. Note that when selecting this heat source model, the heat source field will remain the same as it was defined in the starting time folder (or as it was set by default by the code).


<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on code coupling for 3D simulations</b>
<br><br>At the moment, the fromLatestTime model is the correct heat source model to use if you are coupling OFFBEAT to an external neutronics solver that provides the <C>Q</C> field directly. At every coupled iteration, simply substitute the <C>Q</C> file generated with the external solver in the starting time folder and (re)start the OFFBEAT simulation. 
<br><br>Note that the *starting time folder* does not necessarily correspond to the `0/` folder. It could be a different time folder according to the `startTime` and `startFrom` keywords in the solverDict. Please refer to the OpenFOAM documentation for more information.
</div>

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `fromLatestTime` heat source model in OFFBEAT, the user needs to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  heatSource fromLatestTime;

</code></pre>
</div>	


<!-- </details> -->
