### User documentation for `hyperElasticMisesPlasticCreep` class {#constitutiveLaw_hyperElasticMisesPlasticCreep}

For general instructions on the modeling of the rheology of a material in OFFBEAT see [here](rheology.html).

The `hyperElasticMisesPlasticCreep` constitutive law class implements the StVenant-Kirchoff hyper-elasto-plastic behaviour:

\f[
    S = 2\mu \cdot E + \lambda \cdot tr(E)
\f]
with
\f[
    \sigma = \frac{1}{J}(F \cdot S \cdot F^{T})
\f]
where 
- \\(S \\) is the second Piola-Kirchoff stress tensor
- \\(\sigma \\) is the Cauchy stress tensor
- \\(\mu \\) and \\(\lambda \\) are the lamé parameters
- \\(E \\) is the large strain tensor
- \\(\varepsilon \\) is the small strain tensor
- \\(F \\) is the deformation gradient
- \\(J \\) is the determinant of the deformation gradient

This law is the equivalent of misesPlasticCreep for large strain regimes.
It assumes that the large strain tensor components remain additive:
\f[
    E = E_{el} + E_{creep} + E_{plastic} + E_{swelling} + ..
\f]

The only difference is that the resulting stresses are interpreted as 
StVenant-Kirchhoff stresses, and need to be translated in terms of Cauchy
stress.

As for others neo-Hookean or hyper-elastic behavior, it is mainly implemented in OFFBEAT for benchmark and verification purposes.

For more information, the interested user can refers to the solids4FOAM documentation.

#### Usage  

To use the `neoHookeanMisesPlasticity` constitutive law in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheology byMaterial;
...
materials
{
    ...
    fuel
    {
        material zircaloy;
        ...
        rheologyModel hyperElasticMisesPlasticCreep;

        rheologyModelOptions
        {
            plasticStrainVsYieldStress table
            (
                (0    200e6)
            );
            creepModel LimbackCreepModel;
        }
    }

}
</code></pre>
</div>	
The evolution of the yield stress is declared by the user as a function of the equivalent plastic strain. For a constant yield stress, enter only one line in the table (for example : (0.0 200e6)).

### References
Simo & Hughes, Computational Inelasticity, 1998, Springer.

P. Cardiff, Z. Tukovic, P. De Jaeger, M. Clancy and A. Ivankovic. A Lagrangian cell-centred finite volume method for metal forming simulation, doi=10.1002/nme.5345.


<!-- </details> -->
