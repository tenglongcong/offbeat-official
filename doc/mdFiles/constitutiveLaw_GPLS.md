### User documentation for `GPLS` {#constitutiveLaw_GPLS}

#### Description
For general instructions on the modeling of the rheology of a material in OFFBEAT see [here](rheology.html).

The GPLS (from the names of its authors Gibbon, Poussard and Le Saux) is an anisotropic viscoplastic model with no stress threshold between elastic and vistoplastic regimes (yield stress \f$\sigma_y=0\f$ ). The total strain \f$\underline{\varepsilon}\f$ is classicaly splitted into two additive parts : elastic \f$\underline{\varepsilon}^{el}\f$ and plastic \f$\underline{\varepsilon}^{vp}\f$. The elastic part is assumed isotropic and is described by the Hooke law.

It considers the Hill's equivalent stress (isotropic hardening):\\
\f[   
   \sigma^{\mathrm{H}}_{\mathrm{eq}}=  \sqrt{
    H_F\left(\sigma_{11}-\sigma_{22}\right)^2+
    H_G\left(\sigma_{22}-\sigma_{33}\right)^2+
    H_H\left(\sigma_{33}-\sigma_{11}\right)^2+
    2H_L\sigma_{12}^{2}+
    2H_M\sigma_{13}^{2}+
    2H_N\sigma_{23}^{2}}
\f]


The previous expression can by rewritten by introducing the fourth order Hill tensor noted \f$\underline{\underline{H}}\f$:
\f[
   \sigma^{\mathrm{H}}_{\mathrm{eq}}=
   \sqrt{\underline{\sigma}\,\colon\,\underline{\underline{H}}\,\colon\,\underline{\sigma}}
\f]


The flow rule of the viscoplastic strain rate is given as:
\f[
    \underline{\dot{\varepsilon}}^{vp} = \dot{p}\underline{n}
\f]

\f[
    \dot{p} = \dot{p_{0}} \left(\frac{\sigma^H_{eq}(\underline{\sigma},T,\phi_t)}{\eta(p,T,\phi_t,\zeta)} \right)^{1/m(T,\phi_t)}
\f]

with  the viscoplastic flow direction \f$\underline{n}=\frac{\partial \sigma_H}{\partial \underline{\sigma}}=\underline{\underline{H}}:\frac{\underline{\sigma}}{\sigma^H_{eq}}\f$. \f$\dot{p}\f$ is the equivalent plastic strain rate, m the strain rate sensitivity exponent, \f$dot{p_{0}}\f$ the reference strain rate egual to \f$1.s^{-1}\f$ and \f$\eta\f$ a non linear viscosity coefficient. T is the temperature (K), \f$\phi\f$ the fluence (\f$>1\f$ MeV) and \f$\zeta\f$ the normalized axial position in the fuel rod. We can thus write the viscoplastic strain rate as:

\f[
    \dot{\underline{\varepsilon}^{vp}} = \dot{p}\frac{\partial \sigma_H}{\partial \underline{\sigma}}=\dot{p}\underline{\underline{H}}:\frac{\underline{\sigma}}{\sigma_{eq}^H}
\f]

#### Model parameters
In the anisotropy principal axes, wich coincide with the tube reference system axes \f$(r, \theta, z)\f$, the equivalent stress can be expressed as:
\f[   
   \sigma^{\mathrm{H}}_{\mathrm{eq}}=  \sqrt{
    H_{rr}\left(\sigma_{\theta\theta}-\sigma_{zz}\right)^2+
    H_{\theta\theta}\left(\sigma_{zz}-\sigma_{rr}\right)^2+
    H_{zz}\left(\sigma_{rr}-\sigma_{\theta\theta}\right)^2+
    2H_{r\theta}\sigma_{r\theta}^{2}+
    2H_{rz}\sigma_{rz}^{2}+
    2H_{\theta z}\sigma_{\theta z}^{2}}
\f]

In this system, the Hill's tensor can be visualized as:
\f[
\underline{\underline{H}}=\left(\begin{matrix}
    H_{rr} + H_{zz} & -H_{rr}  & -H_{zz}  & 0 & 0 & 0 \newline
    -H_{rr}  & H_{\theta \theta} + H_{rr} & -H_{\theta \theta}  & 0 & 0 & 0 \newline
    -H_{zz}  & -H_{\theta \theta}  & H_{zz}+H_{\theta \theta} & 0 & 0 & 0 \newline
    0   & 0   & 0   & H_{r \theta} & 0 & 0 \newline
    0   & 0   & 0   & 0 & H_{r z} & 0 \newline
    0   & 0   & 0   & 0 & 0 & H_{\theta z} \newline
    
\end{matrix}\right)
\f]

The anistropy coefficients were determined based on the PROMETRA experimental database. They are given as:
\f[   
    H_{rr}(T, \phi) = 0.485 + 9.5.10^{-2}\frac{1-\boldsymbol{\Phi}(\phi)}{1 + \mathrm{exp}\left[12(\frac{T}{740}-1 \right]} + 0.32 \frac{1-\boldsymbol{\Phi}(\phi)}{1 + \mathrm{exp}\left[10(\frac{T}{760}-1 \right]}
\f]
\f[ 
    H_{z}(T, \phi) = 0.52 + (-0.23+4.10^{-4}T)\frac{1-\boldsymbol{\Phi}(\phi)}{1 + \mathrm{exp}\left[15(\frac{T}{550}-1 \right]} -0.16 \frac{1-\boldsymbol{\Phi}(\phi)}{1 + \mathrm{exp}\left[20(\frac{T}{920}-1 \right]}
\f]
\f[
    H_{rr} + H_{\theta \theta}=1 
\f]
\f[ 
    H_{r\theta},H_{rz},H_{\theta Z} = 1.5
\f]

With:
- T the temperature in Kelvin
- \f$\phi\f$ the fast neutron fluence (\f$10^{25} n/m^2\f$)
- \f$\Phi(\phi)=1-\mathrm{exp}(-0.3\phi)\f$ the damage irradiation function

The strain rate sensitivity exponent \f$m\f$ is given as:
\f[
    m(T, \phi) = \frac{1}{m_0(T)(1-m_{\phi}(T, \phi))}
\f]
where \f$m_0\f$ and \f$m_{\phi}\f$ are given as:
\f[
    m_0(T) = 77.68M_{0T}(T)+4.11(1-M_{0T}(T))
\f]
\f[
    m_{\phi}(T,\phi) = \frac{0.46\Phi(\phi)}{1 + \mathrm{exp} \left[14.49\left(\frac{T}{870} - 1\right)\right]}
\f]

with \f$M_{0T}(T)\f$:
\f[
    M_{0T}(T)=\frac{1}{1 + \mathrm{exp} \left[10.2\left( \frac{T}{692} - 1 \right)\right]}
\f]

The viscosity coefficient \f$\eta\f$ is spllited into a strength coefficient \f$K\f$ and a hardening modulus \f$L\f$. The strength coefficient is given as:

\f[
    K(T, \phi, \zeta) = K_0(T)(1+K_{\phi}(T,\phi,\zeta)
\f]
with:
\f[
    K_0(T) = (1.409\times10^9 - 1.558\times10^6T)K_{0T}(T) + 4.05\times10^7(1-K_{0T}(T))
\f]
\f[
    K_{\phi}(T, \phi, \zeta) = \frac{0.42(1-6.44\times10^{-2}\phi\zeta)\Phi(\phi)}{
                                    1 + \mathrm{exp}\left[19\left(\frac{T}{768}-1 \right)\right]}
\f]
and \f$K_{0T}(T)\f$:
\f[
    K_{0T}(T) = \frac{1}{1 + \mathrm{exp} \left[6.62\left(\frac{T}{1007}-1\right)\right]}
\f]

The hardening modulus \f$L\f$ is heavily dependant on the equivalent viscoplastic strain \f$p\f$ and is given as:
\f[
    L(T, p, \phi) = (p + 1\times10^{-4})^{n_0(T, \phi)} \mathrm{exp}(-\alpha_n(T)p) + (p + 1\times10^{-4})^{n_{\infty}(T,\phi)}\mathrm{exp}(-\alpha_n(T)p)
\f]
with the first exponent \f$n_0\f$ expressed as:
\f[
    n_0(T,\phi) = [7.9\times10^{-2}N_0(T)+3\times10^{-2}(1-N_0(T))] \left( 1 - \frac{0.24\Phi(\phi)}{1 + \mathrm{exp} \left[7 \left( \frac{T}{630}-1\right)\right]}\right)
\f]
\f[
    N_0(T) = \frac{1}{1 + \mathrm{exp} \left[4\left(\frac{T}{620}-1\right)\right]}
\f]
the second exponent \f$n_{\infty}\f$ is given as:
\f[
     n_{\infty}(T,\phi) = [5\times10^{-2}N_{\infty}(T)+5.6\times10^{-3}(1-N_{\infty}(T))] \left( 1 - \frac{0.63\Phi(\phi)}{1 + \mathrm{exp} \left[7 \left( \frac{T}{630}-1\right)\right]}\right)
\f]
\f[
    N_{\infty}(T) = \frac{1}{1 + \mathrm{exp} \left[5\left(\frac{T}{590}-1\right)\right]}
\f]

Finally, the \f$\alpha_n\f$ factor is a function of temperature given as:
\f[
    \alpha_n = 40.45 \mathrm{exp}(2.03\times 10^{-3}T)
\f]

#### Usage

To use the `GPLS` constitutive law in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:
<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheology byMaterial;

materials
{
    ...
    cladding
    {
        material zircaloy;
        ...
        rheologyModel GPLS;
        rheologyModelOptions
        {
            // Fast Neutron fluence
            phi      1;

            // Normalized axial position
            zeta    0.5;

            // Optional. Tolerance for exiting the newton-rahpson iterations (default is 1e-8)
            error 1e-8; 
            // Optional. Max number of newton-rahpson iterations (default is 2500)
            max_iter 2500; 
            // Optional. Relaxation factor for newton raphson iterations (default is 0.1)
            relax 0.1;

            // Optional. Theta value (between 0 and 1) for the integration scheme. 
            0 --> fully explicit (Forward Euler).
            1 --> fully implicit (Backard Euler)
            0.5 --> semi-implicit (mid point rule / Cranck-Nicholson). Default and recommended value.
            theta 1; 

        }
    }
}
</code></pre>
</div>  
Currently, the fast neutron fluence is an user input as we consider that the change over a RIA transient is negligeable.
The \f$\zeta\f$ parameter corresponds to the axial position during base irradiation. If the position from which the simulated rodlet was manufactured is not known, the default value of 0.75 is recommended. The strength coefficient K is weakly dependant of this parameter.

#### References 

A model to describe the anisotropic viscoplastic mechanical behavior of fresh and irradiated Zircaloy-4 fuel claddings under RIA loading conditions, Journal of Nuclear Materials, vol 378.1, 60-69, 2008, https://doi.org/10.1016/j.jnucmat.2008.04.017
    
Le Saux thesis (french - english) : https://www.theses.fr/2008ENMP1608
    
Interested reader may also refers to:
    
The PROMETRA Program: Fuel Cladding Mechanical Behavior under High Strain Rate, Nuclear Technology, vol 157.3, 215-229, 2007, https://doi.org/10.13182/NT07-A3814

Lemaitre, J., & Chaboche, J. (1990). Mechanics of Solid Materials. Cambridge University Press. https://doi.org/10.1017/CBO9781139167970
<!-- </details> -->
