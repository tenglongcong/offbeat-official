### User documentation for `fromLatestTime` burnup class {#burnup_constantBurnup}

For general instructions on the modeling of burnup in OFFBEAT see [here](burnup.html).

The `fromLatestTime` burnup class in OFFBEAT allows you to set the burnup field in a file located the starting time folder. If the <c>Bu</c> file is present in the starting time folder, the internal field and boundary conditions are set according to the <c>Bu</c> file. Otherwise, the burnup field is set to 0 MWd/MT\f$_{oxide}\f$ in each cell, with zeroGradient BCs in all non-empty/-wedge patches. Note that when selecting this burnup model, the burnup field will remain as defined in the starting time folder (or set by default).

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on code coupling for 3D simulations</b>
<br><br>At the moment, the `fromLatestTime` model is the correct burnup model to use if you are coupling OFFBEAT to an external solver that provides the <code>Bu</code> field directly. At every coupled iteration, simply substitute the <code>Bu</code> file generated with the external solver in the starting time folder and (re)start the OFFBEAT simulation. 
<br>Note that the *starting time folder* does not necessarily correspond to the `0/` folder. It could be a different time folder according to the `startTime` and `startFrom` keywords in the solverDict. Please refer to the OpenFOAM documentation for more information.
</div>

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage  

To use the `fromLatestTime` burnup model in OFFBEAT, you will need to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    burnup fromLatestTime;

</code></pre>
</div>	

<!-- </details> -->
