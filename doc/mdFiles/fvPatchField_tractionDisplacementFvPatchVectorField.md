### User documentation for `tractionDisplacement` fvPatchField class {#fvPatchField_tractionDisplacementFvPatchVectorField}

For a list of fvPatchField classes specifically developed for OFFBEAT, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Description

The `tractionDisplacement` boundary condition is used to apply a fixed or time-dependnt traction boundary condition.
The final traction vector is composed of a user defined `traction` (given by the user as a vector) and a `pressure` which is assumed positive when acting against the surface normal vector.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> List of options </summary> -->

#### List of options

The `tractionDisplacement` boundary condition requires the user to specify a few additional parameters in the patch dictionary:

- <b><c>tractionList</c></b> - A time-dependent traction table. Each entry in the table consists of a time step (using the time unit selected in the `solverDict` file) followed by the value of the traction vector at that specific time step in Pa.
- <b><c>traction</c></b> - Fixed traction vector in Pa. It is read only if the previous keyword is absent.
- <b><c>pressureList</c></b> - A time-dependent pressure table. Each entry in the table consists of a time step (using the time unit selected in the `solverDict` file) followed by the value of the pressure at that specific time step.
- <b><c>pressure</c></b> - Fixed pressure in Pa. It is read only if the previous keyword is absent.

- <b><c>planeStrain</c></b> - Activate or deactivate the plane strain approximation for the normal stress at the boundary. **By default, it is set to `false`**. When activated, the normal component of the boundary normal gradient (and thus the normal strain) is assumed constant across the last layer of cells. This approximation is useful for 2D r-z meshes, where the axial discretization of the mesh may not capture the rapid variation of the axial stresses close to the boundary.

- <b><c>fixedSpring</c></b> - Activate or deactivate a fixed spring-dashpot system to improve mechanical convergence. **By default, it is set to `false`**. When activated, the a spring-dashpot force acts on the patch, in addition to the given traction.
- <b><c>fixedSpringModulus</c></b> - Spring modulus in N/m. **Necessary when `fixedSpring` is set to `true`**.
- <b><c>dashpotModulus</c></b> - Dashpot modulus in N/m. **Necessary when `fixedSpring` is set to `true`**.

- <b><c>relax</c></b> - The relaxation factor for the gradient update. **By default, it is set to `1.0`.**

- <b><c>value</c></b> - The initial **displacement** value (not stress value).

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

To apply the `tractionDisplacement` boundary condition to a given patch, you need to specify its typename in the patch definition in the boundary condition file (typically either `0/D` or `0/DD` in case of a simulation that involves incremental mechanical solvers). 
Here is an example where `tractionDisplacement` is applied to the `cladOuter` patch with time-varying traction and pressure (make sure to change `cladOuter` with the appropriate boundary patch name in your case):

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    cladOuter
    {
        type            tractionDisplacement;

        tractionList
        {
            type    table;

            values
            (
                (0.0 (0 0 0))
                (100.0 (-1000 0 0))
            );

            outOfBounds         clamp;     // optional out-of-bounds handling
            interpolationScheme linear;    // optional interpolation method
        }

        pressureList
        {
            type    table;

            values
            (
                (0.0    1e5)
                (100.0  1e7)
            );

            outOfBounds         clamp;     // optional out-of-bounds handling
            interpolationScheme linear;    // optional interpolation method
        }

        // Activate plane strain approximation for the normal stress at the 
        // boundary. It is false by default.
        planeStrain     false;

        // Relaxation factor for the gradient update
        relax           1.0;

        // Initial value
        value           $internalField;
    }

</code></pre>
</div>  

Here is a second example where we use a constant pressure and zero traction:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    cladOuter
    {
        type            tractionDisplacement;

        // Fixed zero traction
        traction        uniform (0 0 0);

        // Fixed pressure
        pressure        uniform 15.5e6;

        // Activate plane strain approximation for the normal stress at the 
        // boundary. It is false by default.
        planeStrain     false;

        // Relaxation factor for the gradient update
        relax           1.0;

        // Initial value
        value           $internalField;
    }

</code></pre>
</div>  

Here is a third example where we use add a fixed spring-dashpot system for stability:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    cladOuter
    {
        type            tractionDisplacement;

        // Fixed zero traction
        traction        uniform (0 0 0);

        // Fixed pressure
        pressure        uniform 15.5e6;

        // Activate plane strain approximation for the normal stress at the 
        // boundary. It is false by default.
        planeStrain     false;

        // Relaxation factor for the gradient update
        relax           1.0;

        // Additional fixed spring-dashpot system
        fixedSpring     on;
        fixedSpringModulus 1e2;
        dashpotModulus     1e4;

        // Initial value
        value           $internalField;
    }

</code></pre>
</div>  


<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on fixed/time-dependent traction and pressure</b>
<br>One can mix in the BC definition time-dependent/fixed pressure with time-dependent/fixed traction.
</div>


<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on incremental solvers</b>
<br>There is no difference in the definition of the `tractionDisplacement` BC when dealing with incremental simulations (i.e. using `DD` instead of `D`).
<br>The traction and pressure required in the dictionary are still the respective total (not incremental) quantities, also in the time-dependent version of the BC.
</div>


<!-- </details> -->