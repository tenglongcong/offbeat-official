### User documentation for `topCladRingPressure` fvPatchField class {#fvPatchField_topCladRingPressureFvPatchVectorField}

For a list of fvPatchField classes specifically developed for OFFBEAT, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Description

The `topCladRingPressure` boundary condition is used for the top cladding annular ring when the top cap is not explicitly part of the model. 
This means that this BC is designed for truncated cladding tube models where the top cap is not included (for instance to save computational time). 

The normal pressure is derived from the following contributions:
- Outer fluid pressure (acting on the downward direction) which can be specified as a fixed value or as a time-dependent list. 
- Gap pressure on the plenum side of the gap (acting in the upward direction)
- Plenum spring pressure (acting in the upward direction)

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br><br>The coolant force as well as the forces resulting from the gap gas and plenum spring act on different surfaces (either on the outer top surface of the cap or on the inner bottom surface of the cap). 
<br><br>Thus the different pressures contributions are internally scaled by the ratio of the areas before being applied to the top cladding ring surface.
For this reason the BC requires from the user the dimensions of the top cap (internal and external diameter).
</div>

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> List of options </summary> -->

#### List of options

The `topCladRingPressure` boundary condition requires the following parameters to be specified in the patch dictionary:

- <b><c>topCapRadius</c></b> - Radius in m of the top cap on the fluid side (outer radius of the cap).
- <b><c>topCapPlenumRadius</c></b> - Radius in m of the top cap on the plenum side (inner radius of the cap.
- <b><c>coolantPressureList</c></b> - A time-dependent coolant pressure table. Each entry in the table consists of a time step (using the time unit selected in the `solverDict` file) followed by the value of the pressure at that specific time step.
- <b><c>coolantPressure</c></b> - Fixed coolant pressure in Pa. It is read only if the previous keyword is absent.

In addition, the `topCladRingPressure` boundary condition inherits the following options from the `tractionDisplacement` class:

- <b><c>planeStrain</c></b> - Activate or deactivate the plane strain approximation for the normal stress at the boundary. **By default, it is set to `false`**. When activated, the normal component of the boundary normal gradient (and thus the normal strain) is assumed constant across the last layer of cells. This approximation is useful for 2D r-z meshes, where the axial discretization of the mesh may not capture the rapid variation of the axial stresses close to the boundary.
- <b><c>relax</c></b> - The relaxation factor for the gradient update. **By default, it is set to `1.0`.**
- <b><c>value</c></b> - The initial **displacement** value (not stress value).

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

To apply the `topCladRingPressure` boundary condition to a given patch, you need to specify its typename in the patch definition in the boundary condition file (typically `0/D` or `0/DD` for simulations involving incremental mechanical solvers). 

Here is an example where `topCladRingPressure` is applied to the `cladTop` patch:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    cladTop
    {
        type            topCladRingPressure;

        topCapRadius       0.00525;
        topCapPlenumRadius 0.0045;

        // Fixed pressure in case the coolantPressureList is not given
        // coolantPressure 1e5;

        coolantPressureList
        {
            type                table;

            // Instead of "file", the user can insert "values"
            file                "$FOAM_CASE/constant/lists/topCladdingRingPressureCoolantList";
    
            // Values can be directly provided instead of an external file
            // values
            // (
            //     (0.0 1e5)
            //     (100.0 1e6)
            // );

            format              foam;      // data format (optional)
            outOfBounds         clamp;     // optional out-of-bounds handling
            interpolationScheme linear;    // optional interpolation method
        }
        
        // Activate plane strain approximation for the normal stress at the 
        // boundary. It is false by default.
        planeStrain     false;

        // Relaxation factor for the gradient update
        relax           1.0;

        // Initial value
        value           $internalField;
    }        

</code></pre>
</div> 

Note that this BC requires the presence of a `plenumSpringPressure` BC on another patch, and that in the respective dictionary, the name of the patch where `topCladRingPressure` is applied figures in the `topCapInnerPatches` list.

For example, the dictionary in the example above might be accompanied by the following definition of `fuelTop` patch:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    fuelTop
    {
        type            plenumSpringPressure;

        springModulus   1e4;
        initialSpringLoading 0;

        fuelTopPatches (fuelTop);
        topCapInnerPatches (cladTop);

        // Activate plane strain approximation for the normal stress at the 
        // boundary. It is false by default.
        planeStrain     false;

        // Relaxation factor for the gradient update
        relax           1.0;

        // Initial value
        value           $internalField;
    }

</code></pre>
</div>  


<!-- </details> -->