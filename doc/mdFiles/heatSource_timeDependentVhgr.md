### User documentation for `timeDependentVhgr` heatSource class {#heatSource_timeDependentVhgr}

For general instructions on the modeling of a heat source in OFFBEAT see [here](heatSource.html).

The `timeDependentVhgr` class in OFFBEAT allows you to set a average **volumetric heat generation rate (vhgr)** in unit of <c>W/m<sup>3</sup></c>. Similar to `timeDependentVhgr`, this class allow users to provide time-dependent vhgr.
Since this class is mainly developed for TRISO, it currently doesn't support the addition of a radial and an axial profile of vhgr (as done in the lhgr heat source classes).


#### Usage

To use the `timeDependentVhgr` heat source model in OFFBEAT, the user needs to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	heatSource timeDependentVhgr;

</code></pre>
</div>

The `timeDependentVhgr` heat source model requires the user to specify a few additional parameters in the `heatSourceOptions` sub-dictionary:\

- <b><c>timePoints</c></b> - A list of time values at which the vhgr is provided. The time unit depend on the userTime selected by the user (seconds by default).
- <b><c>vhgr</c></b> - A list of vhgr values in <c>W/m<sup>3</sup></c>, one value per time-point.
- <b><c>timeInterpolationMethod</c></b> - Select the time interpolation method for time steps that fall in between the time points indicated in the timePoints list.
- <b><c>materials</c></b> - A list of material (or cellZones names) where the heat source model applies.



#### Example

Here is a code snippet of the `solverDict` to be used for the case of a vhgr that ramps to 1e9 <c>W/m<sup>3</sup></c> in 1hr time, remains constant for a year, and then ramps down to 0 in 1 hr.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	heatSource timeDependentVhgr;

	heatSourceOptions
	{
      //           t0  1hr    1yr       1yr+1hr
      timePoints  ( 0   3600 31536000  31539600);
      vhgr        ( 0   1e9  1e9       0       );

      timeInterpolationMethod linear; //step;

	    materials ( fuel );
	}

</code></pre>
</div>


As the time point or data lists might be quite long for a realistic history, making the solver dictionary less readable, the user can create separate files and include them in the heatSourceOptions dictionary using the #include command.
E.g.:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  heatSourceOptions
  {
    timePoints  #include "$FOAM_CASE/constant/lists/timePoints";
    lhgr        #include "$FOAM_CASE/constant/lists/avgPower";
    timeInterpolationMethod linear;

    ...
  }

</code></pre>
</div>


<!-- </details> -->
