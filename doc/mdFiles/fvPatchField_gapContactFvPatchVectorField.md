### User documentation for `gapContact` fvPatchField class {#fvPatchField_gapContactFvPatchVectorField}

For a list of fvPatchField classes specifically developed for OFFBEAT, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details>
  <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Summary

The `gapContact` boundary condition uses the penalty method to handle the contact between two coupled surfaces (of the type `regionCoupledOFFBEAT`). 

When there is no penetration detected between the surfaces, the pressure on both sides of the gap is set equal to the gap pressure.

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br>A `gapGas` model is required for this boundary condition to work properly.
</div>

##### Normal component

If penetration is detected between the surfaces, a contact pressure proportional to the penetration is added to both surfaces, calculated as:

$$
\begin{aligned} 
ip = -f \cdot K \cdot g
\end{aligned}
$$

where:

- \f$ip\f$ is the interface normal pressure.
- \f$g\f$ is the penetration or `gapWidth` (negative if there is penetration).
- \f$K\f$ is a form of surface stifness calculated internally by the code as \f$K_m \cdot A/V\f$, where \f$K_m\f$ is the bulk modulus of the master face and \f$A\f$ and \f$V\f$ are the area and volume of the master cell.
- \f$f\f$ is the penalty scale factor which is introduced to facilitate convergence at the cost of a larger penetration. 

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br>It is important to choose a penalty scale factor that is as close to 1 as allowed by the convergence properties of the case.
</div>

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on the gap width</b>
<br>The gap width is calculated taking into account the location of the face centers and their displacement (`D` for total mechanics solvers, `DD` for incremental solvers).
<br><br>The user can introduce an `offset` to artificially increment the gap size.
</div>

The normal pressure is applied to the slave face and (if the master is not considered as rigid) it is then interpolated to the master face.

##### Tangential or friction component

For the friction component a Coulomb model is used. The magnitude of the friction vector is calculated as:

$$
\begin{aligned} 
\tau = min(\alpha \cdot ip, f_{fr} \cdot \mu \cdot mag(s))
\end{aligned}
$$

where:

- \f$\tau\f$ is the magnitude of the frictional component.
- \f$\alpha\f$ is the friction coefficient
- \f$\mu\f$ is a form of surface stifness calculated internally by the code as \f$\mu_m \cdot A/V\f$, where \f$\mu_m\f$ is the shear modulus of the master face and \f$A\f$ and \f$V\f$ are the area and volume of the master cell.
- \f$s\f$ is the slip i.e. the cumulated tangential distance between the two coupled surfaces (starting from the moment they entered into contact).
- \f$f_{fr}\f$ is the penalty scale factor for the friction component which is introduced to facilitate convergence at the cost of a larger penetration. 

This means that the friction component is never allowed to surpass in magnitude a certain user-defined fraction of the normal pressure (stick/slip conditions).
The friction vector is then oriented in the direction opposite to the slip and summed to the normal pressure component to obtain the total surface traction vector.

The tangential traction is applied to the slave face and (if the master is not considered as rigid) it is then interpolated to the master face.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> List of options </summary> -->

#### List of options

The `gapContact` boundary condition requires the following parameters to be specified in the patch dictionary:

- <b><c>patchType</c></b> - It indicates to OpenFOAM that this patch field underlines to correct fvPatch type. **It must be set to `regionCoupledOFFBEAT`**.
- <b><c>penaltyFactor</c></b> - The penalty scale factor for the interface normal pressure. It should be chosen carefully to balance convergence and penetration. **It is 0.1 by default**. Typical values range between 0.01 to 1.
- <b><c>penaltyFactorFriction</c></b> - The penalty scale factor for the frictional forces at the interface. **It is 0.1 by default**. Typical values range between 0.01 to 1.
- <b><c>frictionCoefficient</c></b> - The coefficient of friction between the two surfaces. **It is 0 by default**. 

- <b><c>relaxInterfacePressure</c></b> - The relaxation factor for the interface normal pressure update. **It is 1 by default**. Typical values range between 0.01 to 1.
- <b><c>relaxFriction</c></b> - The relaxation factor for the frictional forces update. **It is 1 by default**. Typical values range between 0.01 to 1.
- <b><c>offset</c></b> - An offset added to gap width calculation. A positive value increases the gap width. It can be useful to perform sensitivity analysis on the gap size. **It is 0 by default**.

- <b><c>rigidMasterNormal</c></b> - If set to `true`, the normal component is applied only on the slave side when using the rigid master mode. **It is `false` by default**. 
- <b><c>rigidMasterFriction</c></b> - If set to `true`, the friction component is applied only on the slave side when using the rigid master mode. **It is `false` by default**.

In addition, the `gapContact` boundary condition inherits the following options from the `tractionDisplacement` class:

- <b><c>planeStrain</c></b> - Activate or deactivate the plane strain approximation for the normal stress at the boundary. **By default, it is set to `false`**. When activated, the normal component of the boundary normal gradient (and thus the normal strain) is assumed constant across the last layer of cells. This approximation is useful for 2D r-z meshes, where the axial discretization of the mesh may not capture the rapid variation of the axial stresses close to the boundary.
- <b><c>relax</c></b> - The relaxation factor for the gradient update. **By default, it is set to `1.0`.**
- <b><c>value</c></b> - The initial **displacement** value (not stress value).

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

To apply the `gapPressure` boundary condition to a given patch, you need to specify its typename in the patch definition in the boundary condition file (typically either `0/D` or `0/DD` in case of a simulation that involves incremental mechanical solvers).

Here is an example where `gapContact` is applied to the `fuelOuter` and `cladInner` patches:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  "fuelOuter|cladInner"
  {
      type            gapContact;
      patchType       regionCoupledOFFBEAT;

      penaltyFactor   0.01;
      penaltyFactorFriction 0.01;

      frictionCoefficient        0.1;

      relaxInterfacePressure   0.01;
      relaxFriction           0.01;
  
      // Offset for gap width calculation (a positive value increases the gap width)
      offset          0.0;

      // If rigidMasterNormal(Friction) is true, the normal (friction) component
      // is applied only on the slave side
      rigidMasterNormal       false;
      rigidMasterFriction     false;

      // Activate plane strain approximation for the normal stress at the 
      // boundary. It is false by default.
      planeStrain     false;

      // Relaxation factor for the gradient update
      relax           1.0;

      // Initial value
      value           $internalField;
  } 

</code></pre>
</div>  

<!-- </details> -->