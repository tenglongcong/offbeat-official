### User documentation for `PyC` class {#materials_PyC}

The `PyC`class in OFFBEAT allows you to select specific models for PyC material.

#### Usage

To use `PyC` in OFFBEAT, you just need to specify the keyword **PyC** for `material`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        //- Name of cellZone with PyC material model
        "IPyC|OPyC"
        {
            material PyC;
        }

        ...

    }

</code></pre>
</div>

The material property and behavioral models can selected with the specific keywords. Otherwise, default models will be used. All of the models are take from PARFUME theory manual.

  |                             | defaultModel                                              |  Options                                      |
  | ----------------------------| ----------------------------------------------------------| --------------------------------------------- |
  | densityModel                | [constant](@ref Foam.densityConstant)                     | [constant](@ref Foam.densityConstant) |
  | heatCapacityModel           | [constant](@ref Foam.heatCapacityConstant)                | [constant](@ref Foam.heatCapacityConstant) |
  | conductivityModel           | [constant](@ref Foam.conductivityConstant)                | [constant](@ref Foam.conductivityConstant) |
  | YoungModulusModel           | [PyCPARFUME](@ref Foam.YoungModulusPARFUMEPyC)            | [constant](@ref Foam.YoungModulusConstant)<br />[PyCPARFUME](@ref Foam.YoungModulusPARFUMEPyC)  |
  | PoissonRatioModel           | [constant](@ref Foam.PoissonRatioConstant)                | [constant](@ref Foam.PoissonRatioConstant) |
  | thermalExpansionModel       | [PyCPARFUME](@ref Foam.thermalExpansionPARFUMEPyC)        | [constant](@ref Foam.thermalExpansionConstant)<br /> [PyCPARFUME](@ref Foam.thermalExpansionPARFUMEPyC) |
  | swellingModel               | [PyCPARFUME](@ref Foam.swellingPARFUMEPyC)                | [PyCPARFUME](@ref Foam.swellingPARFUMEPyC)<br />[constant](@ref Foam.constantSwelling)<br />[PyCCorrelation](@ref Foam.swellingCorrelationPyC) |
  | creepModel                  | | [PARFUMUEPyCCreepModel](@ref Foam.PARFUMEPyCCreepModel)<br />[constantCreepPrincipalStress](@ref Foam.constantCreepPrincipalStress)<br />[correlationCreepPrincipalStress](@ref Foam.correlationCreepPrincipalStress)|         


#### Examples

Here is a example of the usage of 'PyC'

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em; overflow: scroll; height: 300px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        //- Name of cellZone with PyC material model
        "IPyC|OPyC"
        {
            material PyC;

            densityModel            constant;
            heatCapacityModel       constant;
            conductivityModel       constant;
            YoungModulusModel       PyCPARFUME;
            PoissonRatioModel       constant;
            thermalExpansionModel   PyCPARFUME;
            swellingModel           PyCPARFUME;
            emissivityModel         constant;

            rho         rho     [1 -3 0 0 0]    1880.0;
            Cp          Cp      [0 2 -2 -1 0]   720.0;
            k           k       [1 1 3 -1 0]    4.0;
            nu          nu      [0 0 0 0 0]     0.33;
            emissivity emissivity       [0 0 0 0 0]   0.0;
            Tref        Tref    [ 0 0 0 1 0 ] 298.15;

            asFabricatedAnisotropy       1.063;

            rheologyModel       misesPlasticCreep;
            rheologyModelOptions
            {
                plasticStrainVsYieldStress table
                (
                    (0    1e60)
                );
                relax 1.0;
                creepModel PARFUMUEPyCCreepModel;
                fluxConversionFactor       0.91;
            }

        }

        ...

    }

</code></pre>
</div>
