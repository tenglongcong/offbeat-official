### User documentation for `MatproCreepModel` class {#creepModel_MatproCreepModel}

#### Description
The `MatproCreepModel` implements the computation the creep strain rate for UO2 fuel according to the MATPRO database documentation. The modifications proposed in the BISON fuel perfomance code are implemented.
Fuel creep is modeled as a function of time, temperature, grain size, density, fission rate, oxygen to metal ratio and effective stress.

The constitutive equation of the creep strain rate is given as:
\f[
    \dot{\varepsilon} = \frac{A_1 + A_2 \dot{F}}{(A_3 + D)G^2}\sigma\mathrm{exp}\left( \frac{-Q_1}{RT} \right) + \frac{A_4}{A_6 + D}\sigma^{4.5}\mathrm{exp}\left( \frac{-Q_2}{RT} \right) + A_7\dot{F}\sigma\mathrm{exp}\left (\frac{-Q_3}{RT} \right)
\f]
where:
- \f$dot{\varepsilon}\f$ is the creep strain rate (1/s)
- \f$\sigma \f$ is the effective stress (Von Mises) in Pa
- \f$T \f$ is the temperature in Kelvin
- \f$D \f$ is the fueld density in percentage of the theoretical one
- \f$G \f$ is the grain size in \f$\mu m \f$
- \f$\dot{F} \f$ is the volumetric fission rate (fission/m\f$^{3}\f$/s)
- \f$Q_i\f$ are the activation energies (J/mol)
- \f$R \f$ is the universal gas constant equal to 8.3145 J/mol/K

Each terms correspond to a specific creep phenomena:
- the first term describes diffusional thermal creep in low stress and low temperature conditions
- the second term describes the thermal dislocation creep in high stress and high temperature conditions
- the third term account for irradiation creep at low temperature where thermal creep is not active. Irradiation creep is also included in the first term (function of the fission rate)

The values of the parameters are as follow:
\f[
    \begin{matrix}
        \textbf{Parameter} & \textbf{Value} \newline    
        A_1 & 0.3919 \newline
        A_2 & 1.3100\times 10^{-19} \newline
        A_3 & -87.7 \newline
        A_4 & 2.0391\times 10^{-25} \newline
        A_6 & -90.5 \newline
        A_7 & 7.78\times 10^{-37} \newline
        Q_1 & 74.829f(x) + 3.01.762 \newline
        Q_2 & 83.143f(X) + 469.191 \newline
        Q_3 & 21759
    \end{matrix}
\f]

The function f define the dependence of the activation energies for thermal creep on the OM ratio:
\f[
    f(x) = \frac{1}{\mathrm{exp}\left(\frac{-20}{\mathrm{log}(x-2)} -8 \right) +1}
\f]


#### Usage
To use the `MatproCreepModel` creep model in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
...
materials
{
    material UO2;
    ...
    rheologyModel  misesPlasticCreep;
    rheologyModelOptions
    {
        yieldStressModel    constant;
        // This large value of yield stress translates makes sure
        // that no plasticity is considered for the fuel
        sigmaY 1e60;

        creepModel MatproCreepModel;
    }
}
</code></pre>
</div>	

#### References
BISON documentation:
https://mooseframework.inl.gov/bison/source/materials/tensor_mechanics/UO2CreepUpdate.html#uo-2-creepupdate

MATPRO documentation and original model (FCREEP): 
https://www.osti.gov/servlets/purl/6442256