### User documentation for `solidConduction` class {#thermalSubSolver_solidConductionSolver}

For general instructions on the modeling of heat transfer in OFFBEAT, refer to [here](thermal.html).

In the `solidConduction` solver, the temperature distribution is obtained from the solution of the heat diffusion or energy conservation equation.

#### Formulation

For an arbitrary body with volume V, bounded by a surface S with outward normal n, this equation can be expressed as:

$$
\begin{aligned} 
\int \rho c_p \frac{dT}{dt} dt = \int_S \vec{q''} \cdot \vec{n} dS + \int_V Q dV
\end{aligned}
$$

where:

- \f$\rho\f$ is density kg/m\f$^3\f$
- \f$c_p\f$ is the specific heat capacity in J/kg/K/
- \f$Q\f$ is the volumetric heat source in W/m\f$^3\f$
- \f$q\f$ is the surface heat flux W/m\f$^2\f$

Using Fourier's law, one can express the heat flux in terms of the temperature gradient as:

$$
\begin{aligned} 
\vec{q''} = k \nabla T
\end{aligned}
$$

where \f$k\f$ is thermal conductivity in W/m/k.

Thus, the final form of the equation (applying the Gauss-Green theorem) is:

$$
\begin{aligned} 
\int \rho c_p \frac{dT}{dt} dt = \int_V \nabla(k\nabla T) dV + \int_V Q dV
\end{aligned}
$$

#### Usage

To use the `solidConduction` solver in OFFBEAT, you will need to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
   thermalSolver    solidConduction;

</code></pre>
</div>

As a `thermalSubSolver` class, `solidConduction` requires the user to specify a few additional parameters in the `thermalOptions` sub-dictionary:

- <b><c>heatFluxSummary</c></b> - It activates the calculation of the heat flowing out from every surface of the domain, printing this information on the terminal. **It is set to false by default**.
- <b><c>calculateEnthalpy</c></b> - It activates the calculation of the enthalpy deposited in the fuel materials (useful for RIA cases). **It is set to false by default**.

#### Examples

Here is an example of the `solverDict` to be used for activating the `solidConduction` thermal solver:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
   thermalSolver solidConduction;

   thermalOptions
   {
      // Print summary of boundary-heat fluxes at each iteration
      heatFluxSummary false;

      // Activate calculation of enthalpy - false by default
      calculateEnthalpy false;
   }

</code></pre>
</div>
