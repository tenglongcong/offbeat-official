### User documentation for `coolantPressure` fvPatchField class {#fvPatchField_coolantPressureFvPatchVectorField}

For a list of available fvPatchField classes, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Description

The `coolantPressure` patch field is used to apply a the coolant pressure (fixed or time-dependent) to the patch.
It is a re-implementation of the tractionDisplacement mother class, to allow the user to deal only with the pressure of the coolant (instead than dealing with a traction and pressure).

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> List of options </summary> -->

#### List of options  

The `coolantPressure` patch field requires the following parameters to be specified in the patch dictionary:

- <b><c>coolantPressureList</c></b> - A time-dependent pressure table. Each entry in the table consists of a time step (using the time unit selected in the `solverDict` file) followed by the value of the pressure at that specific time step.
- <b><c>coolantPressure</c></b> - Fixed pressure in Pa. It is read only if the previous keyword is absent.

- <b><c>planeStrain</c></b> - Activate or deactivate the plane strain approximation for the normal stress at the boundary. **By default, it is set to `false`**. When activated, the normal component of the boundary normal gradient (and thus the normal strain) is assumed constant across the last layer of cells. This approximation is useful for 2D r-z meshes, where the axial discretization of the mesh may not capture the rapid variation of the axial stresses close to the boundary.

- <b><c>relax</c></b> - The relaxation factor for the gradient update. **By default, it is set to `1.0`.**

- <b><c>value</c></b> - The initial **displacement** value (not stress value).

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

To apply the `coolantPressure` boundary condition to a given patch, you need to specify its typename in the patch definition in the boundary condition file (typically either `0/D` or `0/DD` in case of a simulation that involves incremental mechanical solvers). 
Here is an example where `coolantPressure` is applied to the `cladOuter` patch with time-varying traction and pressure (make sure to change `cladOuter` with the appropriate boundary patch name in your case):

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    cladOuter
    {
        type            coolantPressure;

        // Fixed pressure in case the coolantPressureList is not given
        // coolantPressure 1e5;

        coolantPressureList
        {
            type                table;

            // Instead of "file", the user can insert "values"
            file                "$FOAM_CASE/constant/lists/topCladdingRingPressureCoolantList";
    
            // Values can be directly provided instead of an external file
            // values
            // (
            //     (0.0 1e5)
            //     (100.0 1e6)
            // );

            format              foam;      // data format (optional)
            outOfBounds         clamp;     // optional out-of-bounds handling
            interpolationScheme linear;    // optional interpolation method
        }

        // Activate plane strain approximation for the normal stress at the 
        // boundary. It is false by default.
        planeStrain     false;

        // Relaxation factor for the gradient update
        relax           1.0;

        // Initial value
        value           $internalField;
    }    

</code></pre>
</div>

<!-- </details> -->