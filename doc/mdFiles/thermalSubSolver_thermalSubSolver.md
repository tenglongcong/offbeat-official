### User documentation for `fromLatestTime` thermal class {#thermalSubSolver_thermalSubSolver}

For general instructions on the modeling of heat transfer in OFFBEAT see [here](thermal.html).

The `fromLatestTime` thermal solver class in OFFBEAT allows you to set the temperature field via the `T`  file located in the starting time folder. If the `T` file is present in the starting time folder, the internal field and boundary conditions are set according to the `T` file. Otherwise, the temperature field is set to 3000 K in each cell, with zeroGradient BCs in all non-empty/-wedge patches. Note that the `T` field will remain the same as it was read from file (or set by default) at the start of the simulation.

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on code coupling for 3D simulations</b>
<br><br>At the moment, the <c>fromLatestTime</c> class is one of two possible choices to couple OFFBEAT to an external solver that provides the <code>T</code> field directly at every time-step. At every coupled iteration, simply substitute the <code>T</code> file generated with the external solver in the starting time folder and (re)start the OFFBEAT simulation. 
<br>Note that the *starting time folder* does not necessarily correspond to the <c>0/</c> folder. It could be a different time folder according to the <c>startTime</c> and <c>startFrom</c> keywords in the solverDict. Please refer to the OpenFOAM documentation for more information.
</div>

#### Usage  

To use the `fromLatestTime` thermal solver in OFFBEAT, the user will need to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
   thermalSolver fromLatestTime;

</code></pre>
</div>   

As a `thermalSubSolver` class, `solidConduction` <u>allows</u> the user to specify a few additional parameters in the `thermalOptions` sub-dictionary:

- <b><c>heatFluxSummary</c></b> - It activates the calculation of the heat flowing out from every surface of the domain, printing this information on the terminal. **It is set to false by default**.
- <b><c>calculateEnthalpy</c></b> - It activates the calculation of the enthalpy deposited in the fuel materials (useful for RIA cases). **It is set to false by default**.

#### Examples

Here is a code snippet of the `solverDict` to be used for activating the `fromLatestTime` thermal class:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
   thermalSolver fromLatestTime;

   thermalOptions
   {
      // Print summary of boundary-heat fluxes at each iteration
      heatFluxSummary false; 

      // Activate calculation of enthalpy - false by default
      calculateEnthalpy false;
   }

</code></pre>
</div>
