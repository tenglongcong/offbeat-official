### User documentation for `fixedTemperature` fvPatchField class {#fvPatchField_fixedTemperatureFvPatchScalarField}

For a list of fvPatchField classes specifically developed for OFFBEAT, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details>
  <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Summary

The `fixedTemperature` boundary condition allows to specify a constant fixed temperature on a patch. 
If an oxide layer is present, an additional thermal resistance corresponding to the oxide is considered in order to set the patch temperature, and the oxideLayer temperature needs to be specified by the user.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### List of options

The `fixedTemperature` boundary condition requires the user to specify a few additional parameters in the patch dictionary:

- <b><c>outerOxideTemperature</c></b> - If the corrosion model is present, this keyword specifies the initial outer temperature (i.e. the one outside the oxide layer). **If not present, the initial oxide outer temperature is set to "value"**
- <b><c>value</c></b> - The initial **temperature** value (in case of oxidation model, it is the outer temperature of the metallic portion of the body).

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->  

#### Examples

To apply the `fixedTemperature` boundary condition to a given patch, the following example can be used as a template:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    claddingOuterSurface
    {
        type            fixedTemperature;

        // "value" is:
        // - the initial metal-oxide interface temperature, if the corrosion model is present
        // - the initial outer temperature, if the corrosion is not present
        value           uniform 300;
                
        outerOxideTemperature           uniform 300;
    }

</code></pre>
</div>  

<!-- </details> -->    
    