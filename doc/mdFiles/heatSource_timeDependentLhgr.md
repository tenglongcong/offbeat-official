### User documentation for `timeDependentLhgr` heatSource class {#heatSource_timeDependentLhgr}

For general instructions on the modeling of a heat source in OFFBEAT see [here](heatSource.html).

Similar to the `constantLhgr` heatSource class, the `timeDependentLhgr` class in OFFBEAT allows you to set a average **linear heat generation rate (lhgr)**. 
The main difference is that the lhgr provided by the user can vary over time.
It is also possible to apply a radial and an axial profile to the lhgr. Note that even if the lhgr is constant over time, the axial and radial profile might change over time, depending on the profile type chosen by the user.

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Formulation </summary> -->

#### Formulation

The final volumetric heat source is derived as a combination of lhgr, radial and axial profiles, as follows: 
$$
\begin{aligned} 
Q(t,r,z) = Q_{avg}(t) \cdot f(r,t) \cdot g(z,t)
\end{aligned}
$$

where:

- \f$r\f$ is the relative radial position (0...1)
- \f$z\f$ is the relative axial position (0...1)
- \f$t\f$ is the current time
- \f$f(r,t)\f$ and \f$g(z,t)\f$ are the radial and axial profiles, respectively
- \f$Q_{avg}\f$ is the average power density in \f$W/m^3\f$.

The radial and axial profiles are expected to be normalized to 1, while the average power density is obtained from:
$$
\begin{aligned}
Q_{avg}(t) = \frac{lhgr(t) \cdot h_{ref}}{\frac{V_{model}}{\alpha}} 
\end{aligned}
$$

where:

- \f$h_{ref}\f$ is the height of the rod in the reference, undeformed geometrY
- \f$V_{model}\f$ is the volume of the fuel as calculated by OpenFOAM in the model
- \f$\alpha\f$ is the fraction of the real geometry modeled in the simulations (e.g. 0.25 if you are using symmetry BCs and you are simulating only a quarter of the rod).

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `timeDependentLhgr` heat source model in OFFBEAT, the user needs to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	heatSource timeDependentLhgr;

</code></pre>
</div>

The `timeDependentLhgr` heat source model requires the user to specify a few additional parameters in the `heatSourceOptions` sub-dictionary:

- <b><c>timePoints</c></b> - A list of time values at which the lhgr is provided. The time unit depend on the userTime selected by the user (seconds by default).
- <b><c>lhgr</c></b> - A list of lhgr values in W/m, one value per time-point.
- <b><c>timeInterpolationMethod</c></b> - Select the time interpolation method for time steps that fall in between the time points indicated in the timePoints list.
- <b><c>materials</c></b> - A list of material (or cellZones names) where the heat source model applies.
- <b><c>axialProfile</c></b> - A sub-dictionary that specifies the type of axial profile.
- <b><c>radialProfile</c></b> - A sub-dictionary that specifies the type of radial profile.

The `timeDependentLhgr` heat source model is designed to calculate the volumetric power density for a cylindrical rod with an initial reference height (i.e., in undeformed geometry) along a given axial direction. For simulations where there is an axis of symmetry, the class can take into consideration the angular fraction of the real rod that is being modeled.

Thus, the class requires the following additional parameters in the `globalOptions` sub-dictionary of the `solverDict` dictionary:

- <b><c>pinDirection</c></b> - The vector of the axial direction (e.g. (0 0 1) for the z-axis).
- <b><c>angularFraction</c></b> - The fraction of the 360 degree angle that is being modeled. **NOTE: Remove this keyword for axisymmetric r-z models, as the angular fraction is automatically calculated from the wedge boundaries. Otherwise, it must be provided. For example, set it to 0.25 for quarter symmetry, to 0.5 for half-symmetry and to 1.0 for 3D simulations**.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

Here is a code snippet of the `solverDict` to be used for the case of a lhgr that ramps to 10 kW/m in 1hr time, remains constant for a year, and then ramps down to 0 in 1 hr. 
This particular example considers  flat (i.e. uniform) axial and radial profiles, and a quarter of a fuel disc model.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	heatSource timeDependentLhgr;

	globalOptions
	{
    	angularFraction 0.25;
    	pinDirection  (0 0 1);
	}

	heatSourceOptions
	{
      //           t0  1hr    1yr       1yr+1hr
      timePoints  ( 0   3600   31536000  31539600);
      lhgr        ( 0   100e2  100e2     0       );

      timeInterpolationMethod linear; //step;

	    materials ( fuel );

	    axialProfile
	    {        
	        type flat;
	    }

	    radialProfile
	    {
	        type flat;
	    }
	} 

</code></pre>
</div>

For a time-dependent axial profile, one can modify the `axialProfile` sub-dictionary as follows:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	heatSourceOptions
	{
		// .. The rest of the dictionary may remain unchanged

		axialProfile
		{        
		    // Example: time dependent axial radial profile. 
		    // The user provides a 2D table (z-vs-time) of normalized lhgr values 
		    // (normalized to the average).
		    // Other options might be available (see axialProfile class).     
		    type            timeDependentTabulated;

		    // List of time points for axial profile
		    timePoints      ( 0 3600 31539600 );

		    // List of axial locations for axial profile
		    axialLocations  ( 0.0 0.5 1.0 );

		    // 2D table for axial profile (z is x axis; time is y axis)
		    data          
		    (
		        ( 1.0 1.0 1.0 )
		        ( 0.5 1.0 1.5 )
		        ( 0.8 1.1 1.0 ) 
		    );

		    // Select the axial interpolation method
		    axialInterpolationMethod    linear;

		    // Select the time interpolation method for time steps that fall in between
		    // the time points indicated in the timePoints list above
		    timeInterpolationMethod     linear;
		}

	}

</code></pre>
</div>  

As the time point or data lists might be quite long for a realistic history, making the solver dictionary less readable, the user can create separate files and include them in the heatSourceOptions dictionary using the #include command. 
E.g.:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  heatSourceOptions
  {
    timePoints  #include "$FOAM_CASE/constant/lists/timePoints";
    lhgr        #include "$FOAM_CASE/constant/lists/avgPower";
    timeInterpolationMethod linear;

    ...
  }

</code></pre>
</div> 


<!-- </details> -->
