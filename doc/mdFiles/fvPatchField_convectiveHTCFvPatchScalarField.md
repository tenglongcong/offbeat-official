### User documentation for `convectiveHTC` fvPatchField class {#fvPatchField_convectiveHTCFvPatchScalarField}

For a list of fvPatchField classes specifically developed for OFFBEAT, please refer to the [fieldsAndBCs](fieldsAndBCs.html) page.

<!-- <details>
  <summary style="font-size: 0.9rem; font-weight: bold;"> Description </summary> -->

#### Summary

The `convectiveHTC` boundary condition allows to simulate an heat sink boundary defined by a convective heat transfer coefficient \f$h\f$ and a sink temperature \f$T_0\f$. The convective heat transfer set by this boundary condition will remain constant in time and spatially along the patch.

This boundary condition computes the temperature \f$T\f$ to be applied to the patch using the following expression for heat flux \f$q"\f$:

\f[
  q" = h\cdot(T-T_0)
\f]

Where \f$q"\f$ is computed from the Fourier's Law:

\f[
  q" = -k\cdot \nabla T
\f]

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Examples </summary> -->

#### Examples

To apply the `convectiveHTC` boundary condition to a given patch, the following example can be used as a template:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  cladOuter
  {
    type            convectiveHTC;

    // Sink temperature in K
    T0              uniform 600;

    // Heat transfer coefficient in W/m^2/K
    h               uniform 50000;
    
    value           uniform 300;
  }    
</code></pre>
</div>  

<!-- </details> -->    
    
