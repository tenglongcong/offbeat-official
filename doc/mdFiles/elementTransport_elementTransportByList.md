### User documentation for `byList` element transport {#elementTransport_elementTransportByList}

For general instructions on the modeling of element transport in OFFBEAT see [here](elementTransport.html).

By selecting the `byList` option for the elementTransport solver, the user can provide a list of the desired [transport solvers](@ref Foam.transportSolver) to be activated for a fuel performance simulation. 

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `byList` element transport option in OFFBEAT, the user will need to specify it in the `solverDict` dictionary using the following syntax:
<br><br>
<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	elementTransport byList;

	elementTransportOptions
	{
		solvers        ( ... );
		
		porosityOptions
		{
			...
		}
	}

</code></pre>
</div>

<br><br>
If `byList` element transport option is selected, OFFBEAT requires also an additional dictionary called `elementTransportOptions` that includes:\

- <b><c>solvers</c></b> - The list of transport solvers that the user wants to enable for the OFFBEAT simulation.

- A subdictionary (one for each solver in <b><c>solvers</c></b>) where the settings of the transport solver can be modified. For further information, please refer to the documentation of each specific transport solver.

The user should not forget that additional entries in the <b><c>system\fvSolutions</c></b> file will be required for each of the activated solvers. Also, additional time-stepping criterion might be required for each activated transport solver in the <b><c>system/controlDict</c></b> file.

<!-- </details> -->
