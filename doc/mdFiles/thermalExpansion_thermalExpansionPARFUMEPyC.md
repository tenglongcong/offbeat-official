### User documentation for `thermalExpansionPARFUMEPyC` class {#thermalExpansion_thermalExpansionPARFUMEPyC}

The `thermalExpansionPARFUMEPyC` models thermal expansion strain of PyC material by the model taken from the code PARFUdME.

#### Formulation

The thermal-expansion coefficients of the PyC in the radial \\(\alpha_r(10^{-6}/K \\) and \\(\alpha_t(10^{-6}/K \\) directions are:
given by:

$$
\begin{aligned}
\alpha_r = (30-37.5\frac{2}{2+BAF})(1+0.11\frac{T-400}{700})
\end{aligned}
$$

$$
\begin{aligned}
\alpha_t = (1+36\frac{1}{(2+BAF)^2})(1+0.11\frac{T-400}{700})
\end{aligned}
$$

where:

- \\(BAF \\) is anisotropy factor,
- \\(T \\) is the temperature(\\(^\circ C \\)).

In 1-D case, the radial and tangential thermal strain \\(\varepsilon_r \\) and \\(\varepsilon_t \\) are provided to \\(xx \\) component and \\(yy, zz \\) components respectively. However, in other cases, we need to convert the strain tensor in spherical coordinate to cartesian coordinate.

$$
\begin{aligned}
\mathbf{\varepsilon_{cart}}=\mathbf{R}\mathbf{\varepsilon_{sphe}}\mathbf{R^T}
\end{aligned}
$$



#### Usage

To use `thermalExpansionPARFUMEPyC` in OFFBEAT, you just need to specify the keyword **PyCPARFUME** for `thermalExpansionModel`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        "IPyC|OPyC"
        {
            material PyC;
            thermalExpansionModel       PyCPARFUME;
        }

        ...

    }

</code></pre>
</div>

#### Input parameters

|                |   Description                   |     Default            |
|----------------|---------------------------------|------------------------|
|asFabricatedAnisotropy  |The as-fabricated anisotropy (dimensionless)  |1.0       |
|sphereCoordinate  |Whether to use spherical coordinate(in 1D case)        |true |


#### TODO
- Add BAF as a field so it can vary with the fast neutron fluence.
