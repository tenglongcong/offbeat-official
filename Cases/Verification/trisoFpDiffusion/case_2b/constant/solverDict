/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Thermal and Mechanical solver selection:
thermalSolver           fromLatestTime;
mechanicsSolver         fromLatestTime;
neutronicsSolver        fromLatestTime;
elementTransport        byList;

//- Material and rhelogy treatment:
materialProperties      byZone;
rheology                byMaterial;

heatSource              fromLatestTime;
burnup                  fromLatestTime;
fastFlux                fromLatestTime;
corrosion               fromLatestTime;
gapGas                  none;
fgr                     none;
sliceMapper             autoAxialSlices;
corrosion               fromLatestTime;

elementTransportOptions
{
  solvers
  (
      FpDiffusion
  );
  FpDiffusionOptions
  {
    outerPatches   outer;
    intialFpConcentration
    {
      Cs       1;
      Kr       0;
    }
  }
}

globalOptions
{
    pinDirection            (0 0 1);
    reactorType             "LWR";
}

thermalSolverOptions
{
    heatFluxSummary     off;
}

rheologyOptions
{
    thermalExpansion off;
}

mechanicsSolverOptions
{
    forceSummary        off;
    cylindricalStress   on;

    multiMaterialCorrection
    {
        type                    uniform;
        defaultWeights          1;
    }
}


materials
{
  Kernel
  {
    material UO2;
    Tref                        Tref [ 0 0 0 1 0 ] 298.15;

    densificationModel          none;
    swellingModel               none;
    relocationModel             none;
    emissivityModel             constant;
    enrichment                  0.025;
    rGrain                      6e-6;
    theoreticalDensity          10960;
    densityFraction             0.95;
    dishFraction                0.0;
    isotropicCracking           off;
    nCracksMax                  12;

    emissivity  emissivity      [0 0 0 0 0]     0.0;

    rheologyModel               elasticity;

    initialCsConcentration      1.0;

  }

  Buffer
  {
    material buffer;

    densityModel                constant;
    conductivityModel           constant;
    heatCapacityModel           constant;
    emissivityModel             constant;
    YoungModulusModel           constant;
    PoissonRatioModel           constant;
    swellingModel               none;

    rho         rho     [1 -3 0 0 0]    1880.0;
    Cp          Cp      [0 2 -2 -1 0]   720.0;
    k           k       [1 1 3 -1 0]    4.0;
    emissivity emissivity [0 0 0 0 0]   1.0;
    E           E       [1 -1 -2 0 0]   3.96e10;
    nu          nu      [0 0 0 0 0]     0.33;
    alpha       alpha   [0 0 0 0 0]     5.5e-6;
    Tref        Tref    [0 0 0 1 0]     1608;

    fissionProductsTransport
    {
      fissionProductsName  ("Cs" "Kr");
      Cs {d1        1e-8;}
      Kr {d1        1e-8;}

    }

    rheologyModel               elasticity;
  }

    IPyC
    {
      material PyC;

      densityModel                constant;
      conductivityModel           constant;
      heatCapacityModel           constant;
      emissivityModel             constant;
      YoungModulusModel           constant;
      PoissonRatioModel           constant;
      swellingModel               none;

      rho         rho     [1 -3 0 0 0]    1880.0;
      Cp          Cp      [0 2 -2 -1 0]   720.0;
      k           k       [1 1 3 -1 0]    4.0;
      emissivity emissivity [0 0 0 0 0]   1.0;
      E           E       [1 -1 -2 0 0]   3.96e10;
      nu          nu      [0 0 0 0 0]     0.33;
      alpha       alpha   [0 0 0 0 0]     5.5e-6;
      Tref        Tref    [0 0 0 1 0]     1608;

      rheologyModel               elasticity;
    }
}

// ************************************************************************* //
