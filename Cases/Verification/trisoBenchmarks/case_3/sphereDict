########################## COMMENT SECTION ##########################
# This input file 'sphereDict' is read by the python script 'sphereMaker.py' in
# order to build the 'blockMeshDict' file for a 1D TRISO model.

# The sphere is reproduced with a computational mesh made of a
# series of blocks. A block is a portion of the mesh with constant geometry
# (radii) and same numerical discretization (number of cells along
# radius).

# Each block is identified with a name, which OFFBEAT uses to set the material
# properties and models. Note that the same name can be used for different
# blocks. In this case, OFFBEAT will group the blocks together under the same
# cellZone/material, that is a group of cells sharing the same material
# properties and behavioral models (see the file solverDict).

# For each block, it is necessary to specify:
# -the thick or the radius;
# -the number of the radial cells;

# The wedge patch types are always set to 'wedge' by default.

# The name of the patches are set by default:
# -bottom, top, front, back (Wedge)
# -outer
# -bufferOuter and ipycInner

# Note 1 :
# Physical dimensions:
# - angles -> degrees

# Note 2 :
# the origin is (0, 0, 0)

# Note 3 :
# The radius direction is x.

# Note 4 :
# Geometrical quantities (heights, radii and offset) must always be written in
# the dictionary as floats (i.e. with the dot followed by decimal digits), even
# if the decimal part is zero: e.g. 5 --> 5.00

########################## INPUT SECTION ##########################

{

# The part of the TRISO to be considered
'Kernel':             False,
'Buffer':             False,
'IPyC':               True,
'SiC':                True,
'OPyC':               False,

# Angle of the wedge, degrees
'wedgeAngle':         0.25,

# Unit conversion (e.g. 0.001 for units in mm)
'convertToMeters':    1e-6,

# Kernel radius and its number of elements
'rKernel':          	250.0,
'meshKernel':         50,

# Buffer thickness and its number of elements
'tBuffer':          	100.0,
'meshBuffer':         20,

# IPyC thickness and its number of elements
'tIPyC':          	  40.0,
'meshIPyC':           8,

# SiC thickness and its number of elements
'tSiC':          	    35.0,
'meshSiC':            8,

# OPyC thickness and its number of elements
'tOPyC':          	  0.0,
'meshOPyC':           8,

}
