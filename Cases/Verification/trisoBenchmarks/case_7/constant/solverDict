/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Thermal and Mechanical solver selection:
thermalSolver           solidConduction;
mechanicsSolver         smallStrain;
neutronicsSolver        fromLatestTime;
elementTransport        fromLatestTime;

//- Material and rhelogy treatment:
materialProperties      byZone;
rheology                byMaterial;

heatSource              fromLatestTime;
burnup                  fromLatestTime;
fastFlux                timeDependentAxialProfile;
corrosion               fromLatestTime;
gapGas                  none;
fgr                     none;
sliceMapper             autoAxialSlices;
corrosion               fromLatestTime;

globalOptions
{
    pinDirection            (0 0 1);
    reactorType             "LWR";
}

thermalSolverOptions
{
    heatFluxSummary     off;
}

rheologyOptions
{
    thermalExpansion off;
}

mechanicsSolverOptions
{
    forceSummary        off;
    cylindricalStress   on;

    multiMaterialCorrection
    {
        type                    uniform;
        defaultWeights          1;
    }
}

fastFluxOptions
{
    timePoints  ( 0     10e7);
    //- change this value so that the object can have the different fluence at 10s.
    //- To verify the variation of E with fluence.
    fastFlux    (3.4722e13  3.4722e13); // n/cm2/s
    timeInterpolationMethod linear;

    axialProfile
    {
        type flat;
    }

    materials (IPyC SiC OPyC);
}

materials
{
    IPyC
    {
      material PyC;
      Tref                        Tref [ 0 0 0 1 0 ] 298.15;

      densityModel                constant;
      conductivityModel           constant;
      heatCapacityModel           constant;
      emissivityModel             constant;
      YoungModulusModel           constant;
      PoissonRatioModel           constant;
      swellingModel               PyCCorrelation;
      radialCoefficients          (-2.12522e-2 1.83715e-2 -5.05553e-3 7.27026e-4 0.0 0.0);
      tangentialCoefficients      (-1.79113e-2 -3.42182e-3 5.03465e-3 -8.88086e-4 0.0 0.0);


      rho         rho     [1 -3 0 0 0]    1900.0;
      Cp          Cp      [0 2 -2 -1 0]   1;
      k           k       [1 1 3 -1 0]    1;
      emissivity emissivity [0 0 0 0 0]   0.0;
      E           E       [1 -1 -2 0 0]   3.96e10;
      nu          nu      [0 0 0 0 0]     0.33;
      alpha       alpha   [0 0 0 0 0]     5.5e-6;
      Tref        Tref    [0 0 0 1 0]     293.0;

      rheologyModel misesPlasticCreep;

      rheologyModelOptions
      {
          plasticStrainVsYieldStress table
          (
              (0    1e60)
          );
          creepCoefficient          2.715e-4;
          relax 1;
          creepModel constantCreepPrincipalStress;
          fluxConversionFactor       1.0;
      }
    }

    SiC
    {
      material constant;

      rho         rho     [1 -3 0 0 0]    3200.0;
      Cp          Cp      [0 2 -2 -1 0]   1;
      k           k       [1 1 3 -1 0]    1;
      emissivity emissivity [0 0 0 0 0]   0.0;
      E           E       [1 -1 -2 0 0]   3.7e11;
      nu          nu      [0 0 0 0 0]     0.13;
      alpha       alpha   [0 0 0 0 0]     4.9e-6;
      Tref        Tref    [0 0 0 1 0]     293.0;

      rheologyModel               elasticity;
    }


    OPyC
    {
      material PyC;
      Tref                        Tref [ 0 0 0 1 0 ] 298.15;

      densityModel                constant;
      conductivityModel           constant;
      heatCapacityModel           constant;
      emissivityModel             constant;
      YoungModulusModel           constant;
      PoissonRatioModel           constant;
      swellingModel               PyCCorrelation;
      radialCoefficients          (-2.12522e-2 1.83715e-2 -5.05553e-3 7.27026e-4 0.0 0.0);
      tangentialCoefficients      (-1.79113e-2 -3.42182e-3 5.03465e-3 -8.88086e-4 0.0 0.0);


      rho         rho     [1 -3 0 0 0]    1900.0;
      Cp          Cp      [0 2 -2 -1 0]   1;
      k           k       [1 1 3 -1 0]    1;
      emissivity emissivity [0 0 0 0 0]   0.0;
      E           E       [1 -1 -2 0 0]   3.96e10;
      nu          nu      [0 0 0 0 0]     0.33;
      alpha       alpha   [0 0 0 0 0]     5.5e-6;
      Tref        Tref    [0 0 0 1 0]     293.0;

      rheologyModel misesPlasticCreep;

      rheologyModelOptions
      {
          plasticStrainVsYieldStress table
          (
              (0    1e60)
          );
          creepCoefficient         2.715e-4;
          relax 1;
          creepModel constantCreepPrincipalStress;
          fluxConversionFactor       1.0;
      }
    }
}

// ************************************************************************* //
