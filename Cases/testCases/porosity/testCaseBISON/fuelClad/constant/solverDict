/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  9
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version         2;
    format          ascii;
    class           dictionary;
    location        "constant";
    object          solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

mechanicsSolver fromLatestTime;

thermalSolver   solidConduction;

neutronicsSolver fromLatestTime;

elementTransport byList;

materialProperties byZone;

rheology        byMaterial;

heatSource      timeDependentLhgr;

burnup          fromLatestTime;

fastFlux        fromLatestTime;

gapGas          none;

fgr             none;

sliceMapper     autoAxialSlices;

corrosion       fromLatestTime;

elementTransportOptions
{
    solvers        ( porosityTransport );
}

globalOptions
{
    pinDirection    ( 0 0 1 );
    reactorType     "LWR";
}

rheologyOptions
{
    modifiedPlaneStrain off;
}

heatSourceOptions
{
    timePoints      ( 0 10000 );
    lhgr            ( 0 50000 );
    timeInterpolationMethod linear;
    axialProfile
    {
        type            flat;
    }
    radialProfile
    {
        type            flat;
    }
    materials       ( fuel );
}

materials
{
    writeMaterialProperties on;
    fuel
    {
        material        UPuO2;
        Tref            Tref [ 0 0 0 1 0 ] 300;
        densificationModel none;
        swellingModel   none;
        relocationModel none;
        enrichment      ;
        rGrain          6e-06;
        theoreticalDensity 10662;
        densityFraction 0.85;
        dishFraction    0;
        isotropicCracking off;
        nCracksMax      12;
        rheologyModel   elasticity;
        heatCapacityModel constant;
        densityModel    constant;
        YoungModulusModel constant;
        PoissonRatioModel constant;
        thermalExpansionModel constant;
        conductivityModel UPuO2Kato;
        rho             rho [ 1 -3 0 0 0 ] 10662;
        Cp              Cp [ 0 2 -2 -1 0 ] 120;
        E               E [ 1 -1 -2 0 0 ] 2e+11;
        nu              nu [ 0 0 0 0 0 ] 0.345;
        alpha           alpha [ 0 0 0 0 0 ] 1e-06;
        Tref            Tref [ 0 0 0 1 0 ] 300;
        oxygenMetalRatio 2;
        ratioUMetal     1;
        ratioPuMetal    0;
        ratioAmMetal    0;
        ratioNpMetal    0;
        isotopesU       ( 1e-05 0.00248 1e-05 0.9975 );
        isotopesPu      ( 0.00872 0.65 0.23512 0.0714 0.03476 );
        isotopesAm      ( 1 0 );
        poreVelocityModel MOXClementFinnis;
        poreVelocityCorrectionFactor 4;
    }
    cladding
    {
        material        steel1515Ti;
        Tref            Tref [ 0 0 0 1 0 ] 300;
        rheologyModel   elasticity;
    }
}


// ************************************************************************* //
