import numpy as np
import matplotlib.pyplot as plt
import sys

# sampleNames = sys.argv[1:]
sampleNames = ["sample1_33mm","sample2_97mm"]

for i in range(len(sampleNames)):

    sampleName = sampleNames[i]

    dataBIS = np.genfromtxt("./{}/dataBISON.csv".format(sampleName),delimiter=",")
    data = np.genfromtxt("./{}/porosity.csv".format(sampleName))
    dataMeshUpd = np.genfromtxt("./meshUpd_{}/porosity.csv".format(sampleName))

    rOut = 2.7
    expHoleDiameter = 0

    if sampleName == "sample1_33mm":
        expHoleDiameter = 1.1138709184177444 # mm
    elif sampleName == "sample2_97mm":
        expHoleDiameter = 0.9730115923634126 # mm

    expHoleRadiusNorm = expHoleDiameter/2.0/rOut

    plt.figure()
    plt.axvspan(0, xmax=expHoleRadiusNorm, color='lightgray', hatch ='///' ,alpha=0.8, label='Central hole - exp')
    plt.plot(dataBIS[:,0],dataBIS[:,1], label='BISON', color='k', alpha=0.7, lw=5)
    plt.plot(data[:,0],data[:,1], label='OFFBEAT', color='r')
    plt.plot(dataMeshUpd[:,0],dataMeshUpd[:,1], label='OFFBEAT - meshUpd', color='b', ls='--')
    plt.xlim([0,1.1])
    plt.xlabel(r"$r/r_{out}$ (-)")
    plt.ylabel("porosity (-)")
    plt.title(sampleName)
    plt.legend()
    plt.savefig("porosity_{}.png".format(sampleName))
