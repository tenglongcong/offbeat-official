0 #  verification (0= no verification)\n
1 #  grain growth (1 = ainscough)\n
1 #  inert gas behavior (1= do IGB)\n
1 #  gas diff coeff (1= Turnbull et al., 1988)\n
1 #  intra bbl evo (1=Pizzocri et al., 2018)\n
1 #  intra bubble_radius (1= Olander&Wongy, 2006)\n
1 #  re-solution (1=Turnbull 1971)\n
1 #  trapping (1= Ham 1958)\n
1 #  nucleation (1= Baker 1971)\n
0 #  DiffSolver (1= SDA, Pizzocri et al., 2019)\n
1 #  format_out (1 = output.txt, values separated by tabs)\n
1 #  gb vac diff coeff (1= Reynolds and Burton, 1979)\n
1 #  gb behavior (1= do InterGranularGasBehavior - Pastore et al., 2013; Barani et al., 2017)\n
1 #  gb micro-cracking (1 = Barani et al., 2017)\n
0 #  hbs formation (0 = non active)\n
0 #  fuel/reactor couple for burnup calculations (0=UO2/PWR)\n
1 #  gas effective (=0) or single atom (=1) diffusion coefficient\n
1 #  add boundary gas sweeping
