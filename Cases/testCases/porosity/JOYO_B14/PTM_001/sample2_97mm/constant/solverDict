/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  9
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version         2;
    format          ascii;
    class           dictionary;
    location        "constant";
    object          solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

thermalSolver           solidConduction;
mechanicsSolver         fromLatestTime;
neutronicsSolver        fromLatestTime;
elementTransport        byList;

materialProperties  byZone;
rheology            byMaterial;
heatSource          timeDependentLhgr;
burnup              fromLatestTime;
neutronics          fromLatestTime;
fastFlux            fromLatestTime;
corrosion           fromLatestTime;
gapGas              none;
fgr                 none;
sliceMapper         autoAxialSlices;


globalOptions
{
    pinDirection    ( 0 0 1 );
    reactorType     ;
}

thermalSolverOptions
{
    heatFluxSummary off;
}

elementTransportOptions
{
    solvers        ( porosityTransport );

    porosityOptions
    {
        updateMesh off;
        boundedAdvectionTerm on;
        diffusiveTerm on;
        coalescenceTerm off;
    }
}

heatSourceOptions
{
    timePoints  ( 0  72000  158040  160200  246600  248400  249000.012  251280 );
    lhgr        ( 0  36350.63  36350.63  40436.15  40436.15  49235.72  49235.72  0 );

    timeInterpolationMethod linear;

    axialProfile
    {
        type flat;
    }

    radialProfile
    {
        type    flat;
    }

    materials ( fuel );
}

fgrOptions
{
    nFrequency 1;
    relax 1;
}

// mechanicsSolverOptions
// {
//     forceSummary        off;  
//     cylindricalStress   on;  
//     RhieChowCorrection   true;
//     RhieChowScaleFactor  1;
//     multiMaterialCorrection
//     {
//         type                    uniform2D;
//         defaultWeights          1;
//     }
//     strainTensor Hencky;

// }


// rheologyOptions
// {
//     thermalExpansion     on;
//     modifiedPlaneStrain  off;
//     planeStress          off;
//     solvePressureEqn     off;
// }

materials
{
    fuel
    {
        material        UPuO2;
        Tref            Tref [ 0 0 0 1 0 ] 300;
        
        densificationModel none;
        swellingModel   none;
        relocationModel none;
        
        enrichment      ;
        rGrain          6e-06;
        theoreticalDensity 11057.75;
        densityFraction 0.8628;
        dishFraction    0;
        
        isotropicCracking off;
        nCracksMax      12;
        
        rheologyModel   elasticity;
        
        conductivityModel UPuO2Kato;
        YoungModulusModel constant;
        poissonRatioModel constant;
        
        E E [1 -1 -2 0 0] 2.493e11;
        nu nu [0 0 0 0 0] 0.317;
        
        oxygenMetalRatio 1.982;
        ratioUMetal     0.6663;
        ratioPuMetal    0.31;
        ratioAmMetal    0.0237;
        ratioNpMetal    0;

        poreDiffusionCoeff 1e-12;
        // poreVelocityModel TestCapVelocity;
        // poreVelocityModel MOXClementFinnis;
        poreVelocityModel MOXLackey;
        // poreVelocityModel UO2Sens;
        // poreVelocityCorrectionFactor 4;
    }
}


// ************************************************************************* //
