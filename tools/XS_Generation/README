#------------------------------------------------------------------------------#
#                                                                              #
#                          README - XS_Generation                              #
#                                                                              #
#------------------------------------------------------------------------------#

This folder contains the SERPENT (v2.1.31) used to generate the microscopic 
effective cross section for the burnup solver of OFFBEAT. 

Content of the folder:
	
	- g1XS : folder containing files for generation of 1-Energy Group
				 averaged XS. Those XS are used in the algorithm for the nuclide
				 depletion.

	- g2XS : folder containing files for generation of 2-Energy Group
				 averaged XS. Thermal group XS are used to compute the
				 absorption macroscopic XS and the total macroscopic XS needed 
				 by the diffusion solver for the solution of the diffusion eq.

Each of these two folders is provided with the following files:
	
	- createCases : bash script to create several Serpent input starting from					the base file 'UO2pellet' at different enrichments.

	- Allrun : bash script to run the previously created Serpent inputs.

	- Allclean : bash script to clean the parent folder.

	- extractXS.py : python script to extract from Serpent results the XS in 
				     OpenFOAM format. 

================================================================================

Example of execution:

  'cd g1XS' -> run 'Allclean' -> run 'Allrun' -> run 'python extractXS.py'

================================================================================
